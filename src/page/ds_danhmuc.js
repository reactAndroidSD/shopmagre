import React, { Component } from 'react';
import { Text, View, StyleSheet, ActivityIndicator, Image, ScrollView, TouchableOpacity ,Alert} from 'react-native';
import { Header, Avatar, Icon } from 'react-native-elements';
import { toUnicode } from 'punycode';


export default class Danhsachdanhmuc extends Component {
  static navigationOptions = {
    title: 'Danh sách danh mục',
  };

  constructor(props) {
    super(props);
    this.state ={ 
      isLoading: true,
       data:[]
    }
  }

  componentDidMount(){
    // 192.168.1.215
     fetch('http://192.168.1.215:3000/danhsachdanhmuc')
      .then(console.log('huhuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuhdiaudhasdhauh'))
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          data : responseJson
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }
  render() {
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }
    return (
      <ScrollView>
      <View>
        <Header
          leftComponent={
            <TouchableOpacity onPress={() =>
              this.props.navigation.openDrawer()}>
              <Image
                style={styles.logo}
                source={require('../../icons/ham.png')}
              />
            </TouchableOpacity>

          }
          centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
        />
        

        {this.state.data.map(data1 => (
          <TouchableOpacity
            onPress = {() => Alert.alert(data1.id_dm.toString())}
          >
       <View style = {a.bao}> 
       <View style = {a.phai}>
         <Text>#{data1.id_dm}</Text> 
         <Text style={{fontSize:25}}>{data1.tendanhmuc}</Text>
         <Text>{data1.madanhmuc}</Text>
       </View>
   </View>
   </TouchableOpacity>
        ))}
      </View>

      </ScrollView>
    );
  }
}
var a = StyleSheet.create({
  bao: {
    flexDirection: "row",
    borderBottomWidth: 1,
    marginBottom: 2,
    padding: 4
  },
  trai: {
    flex: 1,
    flexDirection: "row",
  },
  phai: {
    flex: 5
  },
});

const styles = StyleSheet.create({

  logo: {
      width: 50,
      height: 50,
  },
});