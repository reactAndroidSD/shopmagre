import React, { Component } from 'react';
import { Text, View, StyleSheet, ActivityIndicator, Image, ScrollView, TouchableOpacity } from 'react-native';
import { Header, Avatar, Icon } from 'react-native-elements';

export default class Danhsachphieunhapkho extends Component {
  static navigationOptions = {
    title: 'Danh sách phiếu nhập kho',
  };

  state = { idnhap : -1}

  constructor(props) {
    super(props);
    this.state ={ 
      isLoading: true,
       data:[]
    }
  }

  componentDidMount(){
    // 192.168.1.215
     fetch('http://192.168.1.215:3000/danhsachdonkho_Nhap')
      .then(console.log('huhuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuhdiaudhasdhauh'))
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          data : responseJson
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }

  render() {
    const {navigate} = this.props.navigation
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }
    return (
      <ScrollView>
      <View>
        <Header
          leftComponent={
            <TouchableOpacity onPress={() =>
              this.props.navigation.openDrawer()}>
              <Image
                style={styles.logo}
                source={require('../../icons/ham.png')}
              />
            </TouchableOpacity>

          }
          centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
        />
       
        {this.state.data.map(data1 => (
          <TouchableOpacity
          onPress={() => navigate('Detailkho', { idnhap: data1.id_don , trangthai : data1.trangthai })}

          
          >
            <View key = {data1.id_kh}>
            <View style={a.bao}>
                  <View style={a.phai}>
                    <Text>#{data1.id_don}</Text>
                    <Text style={{ fontSize: 20 }}>{data1.ngaytao.toString().substring(0, 10)}</Text>
                    <View style={{ flexDirection: "row" }}>
                      <View style={{ flex: 3 }}>
                        <Text>Sản phẩm: {data1.id_sp}</Text>
                        <Text style={{ color: "grey" }}>Số lượng:{data1.soluong}</Text>
                      </View>
                    </View>
                  </View>
            </View>
            </View>
                </TouchableOpacity>
          ))}
      </View>
      </ScrollView>

    );
  }
}
var a = StyleSheet.create({
  bao: {
    flexDirection: "row",
    borderBottomWidth: 1,
    marginLeft: 10,
    padding: 4
  },
  trai: {
    flex: 1,
    flexDirection: "row",
  },
  phai: {
    flex: 5
  },
});

const styles = StyleSheet.create({

  logo: {
      width: 50,
      height: 50,
  },
});


// View style={a.bao}>
//                   <View style={a.phai}>
//                     <Text>{rowData.ID}</Text>
//                     <Text style={{ fontSize: 20 }}>{rowData.tenkhachhang}</Text>
//                     <View style={{ flexDirection: "row" }}>
//                       <View style={{ flex: 3 }}>
//                         <Text>{rowData.sodienthoai}</Text>
//                         <Text style={{ color: "grey" }}>{rowData.loaikhachhang}</Text>
//                       </View>
//                       <View style={{ flex: 1 }}>
//                         <Text style={{ textAlign: "right" }}>{rowData.thanhtoan}</Text>
//                       </View>
//                     </View>
//                   </View>
//                 </View>