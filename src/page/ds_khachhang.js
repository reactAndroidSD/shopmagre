import React, { Component } from 'react';
import { Text, View, StyleSheet, ActivityIndicator, Image, ScrollView, TouchableOpacity } from 'react-native';
import { Header, Avatar, Icon } from 'react-native-elements';

export default class ListCustomer extends Component {
  static navigationOptions = {
    title: 'Danh sách khách hàng',
  };
  state = { idkh : 5}


  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: []
    }
  }

  componentDidMount() {
    // 192.168.1.215
    fetch('http://192.168.1.215:3000/danhsachkhachhang')
      .then(console.log('huhuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuhdiaudhasdhauh'))
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          data: responseJson
        }, function () {

        });

      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    const {navigate} = this.props.navigation;
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }
    return (
      <ScrollView>
        <View>
          <Header
            leftComponent={
              <TouchableOpacity onPress={() =>
                this.props.navigation.openDrawer()}>
                <Image
                  style={styles.logo}
                  source={require('../../icons/ham.png')}
                />
              </TouchableOpacity>

            }
            centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
          />
         
          {this.state.data.map(data1 => (
            <TouchableOpacity
            onPress = {() => navigate('Detailkh',{idkh :data1.id_kh})}
            >
              <View key={data1.id_kh}>
                <View style={a.bao}>
                  <View style={a.phai}>
                    <Text>#{data1.id_kh}</Text>
                    <Text style={{ fontSize: 20 }}>{data1.ten}</Text>
                    <View style={{ flexDirection: "row" }}>
                      <View style={{ flex: 3 }}>
                        <Text>{data1.sdt}</Text>
                        <Text style={{ color: "grey" }}>{data1.email}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          ))}
        </View>
      </ScrollView>

    );
  }
}
var a = StyleSheet.create({
  bao: {
    flexDirection: "row",
    borderBottomWidth: 1,
    marginLeft: 10,
    padding: 4
  },
  trai: {
    flex: 1,
    flexDirection: "row",
  },
  phai: {
    flex: 5
  },
});

const styles = StyleSheet.create({

  logo: {
    width: 50,
    height: 50,
  },
});


// View style={a.bao}>
//                   <View style={a.phai}>
//                     <Text>{rowData.ID}</Text>
//                     <Text style={{ fontSize: 20 }}>{rowData.tenkhachhang}</Text>
//                     <View style={{ flexDirection: "row" }}>
//                       <View style={{ flex: 3 }}>
//                         <Text>{rowData.sodienthoai}</Text>
//                         <Text style={{ color: "grey" }}>{rowData.loaikhachhang}</Text>
//                       </View>
//                       <View style={{ flex: 1 }}>
//                         <Text style={{ textAlign: "right" }}>{rowData.thanhtoan}</Text>
//                       </View>
//                     </View>
//                   </View>
//                 </View>