  import React, { Component } from 'react';
  import { Text, View, StyleSheet, ActivityIndicator, Image, ScrollView, TouchableOpacity } from 'react-native';
  import { Header, Avatar, Icon } from 'react-native-elements';
  import { createDrawerNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
  import detail_sanpham from '../detail/detail_sanpham'

  
  export default class ds_sanpham extends Component {
    static navigationOptions = {
      title: 'Danh sách sản phẩm',
    }
    state = { ipsanpham : 5}
    constructor(props) {
      super(props);
      this.state ={ 
        isLoading: true,
         data:[],
      }
    }
    componentDidMount(){
      // 192.168.1.215
       fetch('http://192.168.1.215:3000/danhsachsanpham')
        .then(console.log('huhuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuhdiaudhasdhauh'))
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            isLoading: false,
            data : responseJson
          }, function(){
  
          });
  
        })
        .catch((error) =>{
          console.error(error);
        });
    }
    render() {
      const {navigate} = this.props.navigation;
      if(this.state.isLoading){
        return(
          <View style={{flex: 1, padding: 20}}>
            <ActivityIndicator/>
          </View>
        )
      }
      return (
        <ScrollView>
        <View>
          <View> 
          <Header
          style={{position:"absolute"}}
            leftComponent={
              <TouchableOpacity onPress={() =>
                this.props.navigation.openDrawer()}>
                <Image
                  style={styles.logo}
                  source={require('../../icons/ham.png')}
                />
              </TouchableOpacity>
  
            }
            centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
          />
          </View>
          
         
      
              {this.state.data.map(data1 => (
                <TouchableOpacity
                  onPress = {() => navigate('Detailsp',{ipsanpham :data1.ip_sanpham})}
                >
                <View key = {data1.ip_sanpham}>
              
                  <View style={a.phai}>
                    <Text>{data1.ten_sp}</Text>
                    <Text style={{ color: "grey" }}>{data1.ma_sp}</Text>
                    <View style={{ flexDirection: "row" }}>
                      <View style={{ flex: 1 }}>
                        <Text>Giá vốn: {data1.giavon}</Text>
                        <Text>Giá bán: {data1.giaban}</Text>
                      </View>
                      <View style={{ flex: 1 }}>
                        <Text style={{ textAlign: "right" }}></Text>
                        <Text style={{ textAlign: "right" }}>Tồn:{data1.soluong}</Text>
                      </View>
                    </View>
                  </View>
                
              </View>
              </TouchableOpacity>
            ))}
          
        </View>
        </ScrollView>
  
  
      );
    }
  }
  var a = StyleSheet.create({
    bao: {
      flexDirection: "row",
      borderBottomWidth: 1,
      marginBottom: 2,
      padding: 4
    },
    trai: {
      flex: 1,
      flexDirection: "row",
    },
    phai: {
      borderBottomWidth: 1,
      marginBottom: 2,
      flex: 3,
      padding: 4
    },
  });
  
  const styles = StyleSheet.create({
  
    logo: {
        width: 50,
        height: 50,
    },
  });

  // const DrawerNavigator = createStackNavigator({
  //     Home: {
  //       screen: ds_sanpham
  //     },
  //     Detail:{
  //       screen: detail_sanpham
  //     }
  // })

  // export default StackContainer = createAppContainer(DrawerNavigator)