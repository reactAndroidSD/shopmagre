import React, { Component } from 'react';
import { Text, View, StyleSheet, ListView, TouchableOpacity, Image } from 'react-native';
import { Header, Avatar, Icon } from 'react-native-elements';


export default class Danhsachdonhang extends Component {
  static navigationOptions = {
    title: 'Danh sách đặt hàng',
  };

  state = { iddh : 5}

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: []
    }
  }

  componentDidMount() {
    // 192.168.1.215
    fetch('http://192.168.1.215:3000/danhsachdonhang')
      .then(console.log('huhuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuhdiaudhasdhauh'))
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          data: responseJson
        }, function () {

        });

      })
      .catch((error) => {
        console.error(error);
      });
  }
  render() {
    const {navigate} = this.props.navigation

    return (
      <View>
        <Header
          leftComponent={
            <TouchableOpacity onPress={() =>
              this.props.navigation.openDrawer()}>
              <Image
                style={styles.logo}
                source={require('../../icons/ham.png')}
              />
            </TouchableOpacity>

          }
          centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
        />


        {this.state.data.map(data1 => (
          <TouchableOpacity
            onPress={() => navigate('Detaildonhang', { iddh: data1.id_dh, trangthai: data1.trangthai })}

          >
            <View>
              <View style={a.bao}>
                <View style={a.phai}>
                  <Text>#{data1.id_dh}</Text>
                  <Text style={{ color: "grey" }}>{data1.ten_KH}</Text>
                  <View style={{ flexDirection: "row" }}>
                    <View style={{ flex: 3 }}>
                      <Text>{data1.sdt}</Text>
                      <Text>{data1.gia}</Text>
                    </View>
                  </View>

                </View>
              </View>

            </View>
          </TouchableOpacity>
        ))}
      </View>


    );
  }
}
var a = StyleSheet.create({
  bao: {
    flexDirection: "row",
    borderBottomWidth: 1,
    marginBottom: 2,
    padding: 4
  },
  trai: {
    flex: 1,
    flexDirection: "row",
  },
  phai: {
    flex: 5
  },
});

const styles = StyleSheet.create({

  logo: {
    width: 50,
    height: 50,
  },
});