//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, createDrawerNavigator } from 'react-native';
// import HomeScreen from '../../../screens/HomeScreen';

class DrawerMenu extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}> Drawer Menu </Text>
        </View>
        <ScrollView>
          {this._renderList()}
        </ScrollView>
      </View>
    );
  }

  _renderList = () => this.props.list.map(element => (
    <TouchableOpacity key={element.id} onPress={this._updateElementSelected(element.id)}>
      <Text>Menu title {element.id}</Text>
    </TouchableOpacity>
  ));

  _updateElementSelected = (elementId) => () => {
    this.props.actionSetElementSelected(elementId, this.props.list);
    this.props.navigation.toggleDrawer();
  };
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

class HomeScreen extends React.Component {
  render() {
    return (
      <MyApp />,
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Text>Home Screen</Text>
      </View>
    );
  }
}

//make this component available to the app

// createDrawerNavigator({ Home: {
//   screen: HomeScreen,
// }  }, { contentComponent: DrawerMenu })

const MyDrawerNavigator = createDrawerNavigator({
  Home: {
    screen: HomeScreen,
  },
  // Notifications: {
  //   screen: MyNotificationsScreen,
  // },
},
{ contentComponent: DrawerMenu });

const MyApp = createAppContainer(MyDrawerNavigator);

export default DrawerMenu;
