import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, StatusBar, TouchableOpacity, Image } from 'react-native';

import { createDrawerNavigator, createAppContainer, createStackNavigator } from 'react-navigation';

import { Header, Avatar, Icon } from 'react-native-elements';

// create a component
class SecondScreen extends Component {
    static navigationOptions = {
        title: 'Second',
    };

    constructor(props){
        super(props);
        this.state ={
            a: this.props.navigation.state.params.text
        }
    }


    
    render() {
        const { navigate } = this.props.navigation;
        
        return (
            <View style={styles.container}>
                <Text>
                    {this.state.a}
                </Text>

            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },
});

//make this component available to the app
export default SecondScreen;
