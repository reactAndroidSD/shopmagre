import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, StatusBar, TouchableOpacity, Image, Alert } from 'react-native';

import { createDrawerNavigator, createAppContainer, createStackNavigator } from 'react-navigation';

import { Header, Avatar, Icon } from 'react-native-elements';

import ActionButton from 'react-native-action-button';





import { FloatingAction } from 'react-native-floating-action';

import drawer1 from '../../drawers/drawer1'
import drawer2 from '../../drawers/drawer2'
import drawer3 from '../../drawers/drawer3'
import drawer4 from '../../drawers/drawer4'
import drawer5 from '../../drawers/drawer5'
import drawer6 from '../../drawers/drawer6'
import drawer7 from '../../drawers/drawer7'
class DrawerClass extends Component {

    static navigationOptions = {
        title: 'Home',
    };

    render() {
        const { navigate } = this.props.navigation;

        return (

            <View style={styles.container}>



                <Header
                    leftComponent={
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.openDrawer()}>
                            <Image
                                style={styles.logo}
                                source={require('../../../icons/ham.png')}
                            />
                        </TouchableOpacity>

                    }
                    centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                />



                {/* <StackContainer /> */}
                <Button
                    title='this is the BUTT'
                    onPress={() =>
                        this.props.navigation.openDrawer()}

                />


                {/* <FloatingAction
                    actions={actions}
                    onPressItem={
                        (name) => {
                            if(name == 'bt_videocam')
                            {
                                Alert.alert("button video");
                            }
                            console.log(`selected button: ${name}`);
                        }
                    }
                /> */}
                <ActionButton
                    buttonColor="rgba(231,76,60,1)"
                    onPress={() => { Alert.alert("hello baby") }}
                />








            </View >





        );
    }
}

class Class1 extends Component {
    static navigationOptions = {
        title: 'man1',
    };

    render() {
        return (
            <View >
                <Header
                    leftComponent={
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.openDrawer()}>
                            <Image
                                style={styles.logo}
                                source={require('../../../icons/ham.png')}
                            />
                        </TouchableOpacity>

                    }
                    centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                />
                <Text>CLASS 1</Text>





            </View>
        );
    }
}

class Class2 extends Component {
    static navigationOptions = {
        title: 'man2',
    };

    render() {
        return (
            <View >
                <Header
                    leftComponent={
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.openDrawer()}>
                            <Image
                                style={styles.logo}
                                source={require('../../../icons/ham.png')}
                            />
                        </TouchableOpacity>

                    }
                    centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                />
                <Text>CLASS 2</Text>
            </View>
        );
    }
}

class Class3 extends Component {
    static navigationOptions = {
        title: 'man3',
    };

    render() {
        return (
            <View >
                <Header
                    leftComponent={
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.openDrawer()}>
                            <Image
                                style={styles.logo}
                                source={require('../../../icons/ham.png')}
                            />
                        </TouchableOpacity>

                    }
                    centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                />
                <Text>CLASS 3</Text>
            </View>
        );
    }
}

class Class4 extends Component {
    static navigationOptions = {
        title: 'man4',
    };
    render() {
        return (
            <View >
                <Header
                    leftComponent={
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.openDrawer()}>
                            <Image
                                style={styles.logo}
                                source={require('../../../icons/ham.png')}
                            />
                        </TouchableOpacity>

                    }
                    centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                />
                <Text>CLASS4</Text>
            </View>
        );
    }
}

const MyStackNavigator = createStackNavigator({

    man1: { screen: Class1 },
    man2: { screen: Class2 },
    man3: { screen: Class3 },
    man4: { screen: Class4 },
}, {
        headerMode: 'none'
    }
)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        // backgroundColor: '#3498db',
        //position: 'absolute',
    },
    logo: {
        width: 50,
        height: 50,
    },
    floatingButton: {
        alignSelf: 'flex-end',
        bottom: 35,
        flex: 1
    }
});

const actions = [{
    text: 'Accessibility',
    icon: require('../../../images/ic_accessibility_white.png'),
    name: 'bt_accessibility',
    position: 2
}, {
    text: 'Language',
    icon: require('../../../images/ic_language_white.png'),
    name: 'bt_language',
    position: 1
}, {
    text: 'Location',
    icon: require('../../../images/ic_room_white.png'),
    name: 'bt_room',
    position: 3
}, {
    text: 'Video',
    icon: require('../../../images/ic_videocam_white.png'),
    name: 'bt_videocam',
    position: 4
}];

const StackContainer = createAppContainer(MyStackNavigator)



const MyDrawerNavigator = createDrawerNavigator({
    Home: {
        screen: DrawerClass,
    },
    "Quản lý sản phẩm": {
        screen: drawer1,
    },
    "Quản lý khách hàng": {
        screen: drawer2,
    },
    "Quản lý đơn hàng": {
        screen: drawer3,
    },
    "Quản lý kho hàng": {
        screen: drawer4,
    },
    "Quản lý danh mục": {
        screen: drawer5,
    },
    "Quản lý vận chuyển": {
        screen: drawer6,
    },
    "Quản lý nhà sản xuất": {
        screen: drawer7,
    },
});

const DrawerContainer = createAppContainer(MyDrawerNavigator)

export default DrawerContainer;




