import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, StatusBar, TouchableOpacity, Image, TouchableHighlight } from 'react-native';

import { createDrawerNavigator, createAppContainer,DrawerActions, createStackNavigator } from 'react-navigation';

import { Header, Avatar, Icon } from 'react-native-elements';


// create a component
class HeaderCus extends Component {
    constructor(props){
        super(props)}
    render() {
        return (
            <View>
                <Header
                    leftComponent={
                        <TouchableOpacity >
                            <Image
                            style={styles.logo}
                            source={require('../../../icons/ham.png')}
                        />      
                        </TouchableOpacity>
                                       
                    }
                    centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({

    logo: {
        width: 50,
        height: 50,
    },
});

//make this component available to the app
export default HeaderCus;
