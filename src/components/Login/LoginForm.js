//import liraries
import React, { Component } from 'react';
import App from '../../../App';
import {createStackNavigator, createAppContainer, createDrawerNavigator} from 'react-navigation';
import {Alert, Button, View, Text, StyleSheet, TextInput, TouchableOpacity, KeyboardAvoidingView, StatusBar } from 'react-native';






// create a component
class LoginForm extends Component {
    
    constructor(props) {
        super(props)
    }
    
   

    render() {

        const { navigate } = this.props.navigation;
        

        

        return (
            
            <View behavior="padding" style={styles.container}>
                
                <StatusBar
                    barStyle = "light-content" 
                />


                

                <TextInput 
                placeholder = "username or email"
                placeholderTextColor = 'rgba(255,255,255,0.5)'
                returnKeyType = "next"
                autoCapitalize = "none"
                autoCorrect ={false}
                style={styles.input} />

                <TextInput
                placeholder = "password" 
                placeholderTextColor = 'rgba(255,255,255,0.5)'
                secureTextEntry
                returnKeyType ="go"
                autoCapitalize = "none"
                autoCorrect ={false}
                style={styles.input} />

                
               
                <TouchableOpacity style = {styles.buttonContainer}
                onPress={() =>
                    this.props.navigation.openDrawer()}>

                <Text style = {styles.buttonText}>LOGIN</Text>
                </TouchableOpacity>

              
                

                
            </View>
        );
    }
    
}



const MyDrawerNavigator = createDrawerNavigator({
    Home: {
        screen: LoginForm,
    },
    
});

const DrawerContainer = createAppContainer(MyDrawerNavigator)


export default DrawerContainer;













// define your styles
const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    input: {
        height: 40,
        backgroundColor: 'rgba(255,255,255,0.2)',
        marginBottom: 20,
        color: '#FFF',
        paddingHorizontal: 10,

    },
    buttonContainer: {
        backgroundColor: '#2980b9',
        paddingVertical: 15,
    },
    buttonText: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight: '700',
    }
});


