//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native';

import { createDrawerNavigator, createAppContainer, createStackNavigator } from 'react-navigation';

import LoginForm from './LoginForm';

import Drawer2 from '../TestDrawer/Drawer2'

import { FloatingAction } from 'react-native-floating-action';


// create a component
class Login extends Component {
    static navigationOptions = {
        title: 'Login',
    };
    constructor(props) {
        super(props)
    }
    render() {
        const { navigate } = this.props.navigation;

        return (
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image
                        style={styles.logo}
                        source={require('../images/Shopping.png')}
                    />

                    <Text style={styles.title}>Ứng dụng quản lý bán hàng</Text>


                </View>

                <View style={styles.formContainer}>
                    {/* <LoginForm /> */}
                    <TextInput
                        placeholder="username or email"
                        placeholderTextColor='rgba(255,255,255,0.5)'
                        returnKeyType="next"
                        autoCapitalize="none"
                        autoCorrect={false}
                        style={styles.input} />

                    <TextInput
                        placeholder="password"
                        placeholderTextColor='rgba(255,255,255,0.5)'
                        secureTextEntry
                        returnKeyType="go"
                        autoCapitalize="none"
                        autoCorrect={false}
                        style={styles.input} />

                    <TouchableOpacity style={styles.buttonContainer}
                        // onPress={() =>
                        //     this.props.navigation.openDrawer()}
                        onPress={() => navigate('Drawer2')}
                    >



                        <Text style={styles.buttonText}>LOGIN</Text>
                    </TouchableOpacity>

                </View>
                {/* <FloatingAction
                    actions={actions}
                    onPressItem={
                        (name) => {
                            console.log(`selected button: ${name}`);
                        }
                    }
                /> */}


            </View>
        );
    }
}



// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3498db'
    },
    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center',
    },
    logo: {
        width: 100,
        height: 100,
    },
    title: {
        color: '#FFF',
        textAlign: 'center',
        marginTop: 10,
        width: 200,
        opacity: 1.2
    },
    formContainer: {

    },
    buttonContainer: {
        backgroundColor: '#2980b9',
        paddingVertical: 15,
    },
    buttonText: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight: '700',
    },
    input: {
        height: 40,
        backgroundColor: 'rgba(255,255,255,0.2)',
        marginBottom: 20,
        color: '#FFF',
        paddingHorizontal: 10,

    },

});

//make this component available to the app

//for floating button
const actions = [{
    text: 'Accessibility',
    icon: require('../../../images/ic_accessibility_white.png'),
    name: 'bt_accessibility',
    position: 2
}, {
    text: 'Language',
    icon: require('../../../images/ic_language_white.png'),
    name: 'bt_language',
    position: 1
}, {
    text: 'Location',
    icon: require('../../../images/ic_room_white.png'),
    name: 'bt_room',
    position: 3
}, {
    text: 'Video',
    icon: require('../../../images/ic_videocam_white.png'),
    name: 'bt_videocam',
    position: 4
}];



const MyStackNavigator = createStackNavigator({

    Home: { screen: Login },
    Drawer2: { screen: Drawer2 },
}, {
        headerMode: 'none'
    }
)

const StackContainer = createAppContainer(MyStackNavigator)

export default StackContainer;

