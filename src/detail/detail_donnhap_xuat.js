import React, { Component } from 'react';
import { Text, View, StyleSheet, ListView, Image, ScrollView,Alert } from 'react-native';
import ActionButton from 'react-native-action-button';
import { Header, Avatar, Icon } from 'react-native-elements';


export default class detail_donnhap_xuat extends Component {
  static navigationOptions = {
    title: 'Chi tiết đơn nhập xuất',
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: [],
      data_danhmuc: [],
      nhap: this.props.navigation.state.params.idnhap,
      xuat: this.props.navigation.state.params.idxuat,
      var: 10,
      trangthai: this.props.navigation.state.params.trangthai,


    }
  }

  huydonkho() {
    if(this.state.trangthai == "Đã Huỷ"){
      Alert.alert("Đơn kho đã được hủy trước đó")
    }
    else{
      Alert.alert(
        'Xác nhận hủy đơn kho',
        'Bạn đã chắc chắn chưa?',
      [
        { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: 'OK', onPress: () => fetch('http://192.168.1.215:3000/capnhat_huy_donkho', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              id_don: this.state.var,
            }),
          })
        },
      ],
      { cancelable: false }
    )
   
    }
   
  }
  componentDidMount() {
    if (this.state.nhap > 0) {
      this.state.var = this.state.nhap
    }
    else if (this.state.xuat > 0) {
      this.state.var = this.state.xuat
    }
    else {
      this.state.var = 2
    }
    let data_send = {
      method: 'POST',
      body: JSON.stringify({
        id_dh: this.state.var,
      }),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }
    fetch('http://192.168.1.215:3000/chitietdonkho_byid', data_send)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          data: responseJson
        }, function () {

        });

      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    return (
      <ScrollView>

        <ActionButton buttonColor="rgba(231,76,60,1)">
          {/* <ActionButton.Item buttonColor='#9b59b6' title="Xác nhận" onPress={() => { }}>
            <Icon name="md-create" style={a.actionButtonIcon} />
          </ActionButton.Item> */}
          <ActionButton.Item buttonColor='#3498db' title="Hủy" onPress={() => this.huydonkho()}>
            <Icon name="md-notifications-off" style={a.actionButtonIcon} />
          </ActionButton.Item>
        </ActionButton>


        {this.state.data.map(data1 => (
          <View key={data1.id_dh}>

            <View style={{ flexDirection: "row", paddingTop: 50, paddingLeft: 10, paddingBottom: 10 }}>
              <Text style={{ fontSize: 20 }}>Thông Tin Sản Phẩm</Text>
            </View>
            <View style={{ height: 10, backgroundColor: "white" }}></View>
            <View style={a.viewAttitude}>
              <View style={a.atiitudeRow}>
                <Text style={{ fontSize: 30 }}>Tổng tiền</Text>
              </View>
              <View style={a.atiitudeRow}>
                <Text style={{ fontSize: 20, textAlign: "right" }}>{data1.tongtien}</Text>
                <Text style={{ color: "grey", textAlign: "right" }}>{data1.loaidon} - {data1.trangthai}</Text>
              </View>
            </View>
            <View style={{ height: 10, backgroundColor: "white" }}></View>
            <View>
              {data1.sanpham_info.map(data2 => (
                <View key={data2.ten_sp} style={{ flexDirection: "column", borderBottomWidth: 1 }}>
                  {/* Show thông tin sản phẩm */}
                  <View style={a.viewAttitude_3}>
                    <Text>{data2.ten_sp}</Text>
                    <Text style={a.nameAttitude}>{data2.ma_sp}</Text>
                  </View>
                  <View style={a.viewAttitude_2}>
                    <View style={a.atiitudeRow}>
                      <Text>Số lượng:{data1.soluong}</Text>
                    </View>
                  </View>
                  <View style={a.viewAttitude_2}>
                    <View style={a.atiitudeRow}>
                      <Text>Giá:{data2.giaban}</Text>
                    </View>
                  </View>
                  <View style={a.viewAttitude_2}>
                    <View style={a.atiitudeRow}>
                      <Text>Tiền hàng:{data2.giaban}</Text>
                    </View>
                    <View style={a.atiitudeRow}>
                      <Text style={{ fontSize: 20 }}>Tổng:{data1.tongtien}</Text>
                    </View>
                  </View>
                  <View style={a.viewAttitude_2}>
                    <View style={a.atiitudeRow}>
                      <Text>Màu sắc:{data2.mausac}</Text>
                    </View>
                    <View style={a.atiitudeRow}>
                      <Text>Trọng lượng:{data2.trongluong}g</Text>
                    </View>
                  </View>
                  <View style={a.viewAttitude_2}>
                    <View style={a.atiitudeRow}>
                      <Text>Bảo hành:{data2.baohanh}</Text>
                    </View>
                    <View style={a.atiitudeRow}>
                      <Text>Mô tả:{data2.mota}</Text>
                    </View>
                  </View>
                </View>
              ))}
            </View>
            <View style={{  flexDirection: "row", paddingTop: 50, paddingLeft: 10, paddingBottom: 10 }}>
              <Text style={{ fontSize: 20 }}>Thông tin khách hàng (Xuất kho)</Text>
            </View>
            <View style={a.viewAttitude}>
              <View style={a.atiitudeRow}>
                <Text style={a.nameAttitude}>Tên</Text>
              </View>
              <View style={a.atiitudeRow}>
                <Text style={a.rightText}>{data1.tenkh}</Text>
              </View>
            </View>
            <View style={a.viewAttitude}>
              <View style={a.atiitudeRow}>
                <Text style={a.nameAttitude}>Điện thoại</Text>
              </View>
              <View style={a.atiitudeRow}>
                <Text style={a.rightText}>{data1.sdt}</Text>
              </View>
            </View>
            <View style={a.viewAttitude}>
              <View style={a.atiitudeRow}>
                <Text style={a.nameAttitude}>Địa chỉ</Text>
              </View>
              <View style={a.atiitudeRow}>
                <Text style={a.rightText}>{data1.diachi}</Text>
              </View>
            </View>
            <View style={{ flexDirection: "row", paddingTop: 50, paddingLeft: 10, paddingBottom: 10 }}>
              <Text style={{ fontSize: 20 }}>Thông tin nhà sản xuất (Nhập kho)</Text>
            </View>
            {data1.nhasanxuat_info.map(data3 => (
              <View>
                <View style={a.viewAttitude}>
                  <View style={a.atiitudeRow}>
                    <Text style={a.nameAttitude}>Tên nhà sản xuất</Text>
                  </View>
                  <View style={a.atiitudeRow}>
                    <Text style={a.rightText}>{data3.tennsx}</Text>
                  </View>
                </View>
                <View style={a.viewAttitude}>
                  <View style={a.atiitudeRow}>
                    <Text style={a.nameAttitude}>Số điện thoại</Text>
                  </View>
                  <View style={a.atiitudeRow}>
                    <Text style={a.rightText}>{data3.sdt}</Text>
                  </View>
                </View>
                <View style={a.viewAttitude}>
                  <View style={a.atiitudeRow}>
                    <Text style={a.nameAttitude}>Email</Text>
                  </View>
                  <View style={a.atiitudeRow}>
                    <Text style={a.rightText}>{data3.email}</Text>
                  </View>
                </View>
                <View style={a.viewAttitude}>
                  <View style={a.atiitudeRow}>
                    <Text style={a.nameAttitude}>Địa chỉ</Text>
                  </View>
                  <View style={a.atiitudeRow}>
                    <Text style={a.rightText}>{data3.diachi}</Text>
                  </View>
                </View>


              </View>
            ))}

          </View>
        ))}

      </ScrollView>


    );
  }
}
var a = StyleSheet.create({
  bao: {
    flexDirection: "row",
    borderBottomWidth: 1,
    marginBottom: 2,
    padding: 4
  },
  trai: {
    flex: 1,
    flexDirection: "row",
  },
  phai: {
    flex: 3
  },
  amount: {
    flex: 1,
    borderWidth: 1,
    borderColor: "gray",
    padding: 15
  },
  nameAttitude: {
    color: "gray"
  },
  viewAttitude: {
    borderBottomWidth: 1,
    padding: 10,
    marginTop: 5,
    flexDirection: "row"
  },
  nameCustomer: {
    color: "gray",
    textAlign: "center",
    flex: 1,
    borderWidth: 1
  },
  atiitudeRow: {
    flex: 1,
  },
  rightText: {
    textAlign: "right"
  },
  viewAttitudeColumn: {
    padding: 10,
    marginTop: 5,
    flexDirection: "column"
  },
  viewAttitude_2: {
    padding: 10,
    marginTop: 5,
    flexDirection: "row"
  },
  viewAttitude_3: {
    padding: 10,
    marginTop: 5,
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});

