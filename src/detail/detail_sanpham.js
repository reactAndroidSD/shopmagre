import React, { Component } from 'react';
import { Text, View, StyleSheet, ListView, Image, ScrollView, Alert } from 'react-native';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import { Header, Avatar, Icon } from 'react-native-elements';
import { FloatingAction } from 'react-native-floating-action';
import ActionButton from 'react-native-action-button';


export default class detail_sanpham extends Component {
  static navigationOptions = {
    title: 'Chi tiết sản phẩm',
  };
  state = { idsp: 5 }


  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: [],
      data_danhmuc: [],
      hello: this.props.navigation.state.params.ipsanpham
      // id_sp: this.props.navigation.state.params.id_sp

    }
  }
  componentDidMount() {
    //Alert.alert(this.state.hello.toString())
    let data_send = {
      method: 'POST',
      body: JSON.stringify({
        ip_sanpham: this.state.hello,
      }),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }
    fetch('http://192.168.1.215:3000/chitietsanpham_byid', data_send)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          data: responseJson,
          data_danhmuc: responseJson.danhmuc_info
        }, function () {

        });

      })
      .catch((error) => {
        console.error(error);
      });


    let data_send2 = {
      method: 'POST',
      body: JSON.stringify({
        ip_sanpham: this.state.hello,
      }),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }
    fetch('http://192.168.1.215:3000/sanpham_cotheban', data_send2)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          data_cotheban: responseJson,
        }, function () {

        });

      })
      .catch((error) => {
        console.error(error);
      });
  }
  render() {
    const { navigate } = this.props.navigation;

    return (
      <ScrollView>
        {/* <FloatingAction
          actions={actions}
          onPressItem={
            (name) => {
              if (name == 'bt_accessibility') {
                // Alert.alert("button video");
                navigate('Upsanpham',{idsp : this.state.hello})

              }
              console.log(`selected button: ${name}`);
            }
          }
        /> */}



        <ActionButton buttonColor="rgba(231,76,60,1)">
          <ActionButton.Item buttonColor='#9b59b6' title="Cập Nhật Thông Tin" onPress={() =>  navigate('Upsanpham', {idsp : this.state.hello}) }>
            <Icon name="md-create" style={styles.actionButtonIcon} />
          </ActionButton.Item>
        </ActionButton>

        {this.state.data.map(data1 => (
          <View>
            <View style={{ flexDirection: "column" }}>
              {/* Show số tồn */}
              <View style={{ flexDirection: "column" }}>
                <View style={{ flexDirection: "row" }}>
                  <View style={a.amount}>
                    <Text>TỒN</Text>
                    <Text>{data1.soluong}</Text>
                  </View>
                  <View style={a.amount}>
                    <Text>CÓ THỂ BÁN</Text>
                    <Text>{this.state.data_cotheban}</Text>
                  </View>
                </View>
              </View>
              {/* Show hình */}
              <View style={{ backgroundColor: "white", flexDirection: "row", position: "relative" }}>
                <Image source={{ uri: 'https://pbs.twimg.com/profile_images/486929358120964097/gNLINY67_400x400.png' }}
                  style={{ width: 100, height: 100 }}
                />
              </View>
              {/* Show thông tin sản phẩm */}
              <View style={{ flexDirection: "column" }}>
                <View style={a.viewAttitude}>
                  <Text style={a.nameAttitude}>Tên sản phẩm</Text>
                  <Text>{data1.ten_sp}</Text>
                </View>
                <View style={a.viewAttitude}>
                  <Text style={a.nameAttitude}>Mã sản phẩm</Text>
                  <Text>{data1.ma_sp}</Text>
                </View>
                <View style={a.viewAttitude}>
                  <Text style={a.nameAttitude}>Danh mục</Text>
                  <Text>{data1.danhmuc_info.map(data2 => (data2.tendanhmuc))}</Text>
                </View>
                <View style={a.viewAttitude}>
                  <Text style={a.nameAttitude}>Giá bán lẻ</Text>
                  <Text>{data1.giaban} VND</Text>
                </View>
                <View style={a.viewAttitude}>
                  <Text style={a.nameAttitude}>Giá bán buôn</Text>
                  <Text>{data1.giabuon} VND</Text>
                </View>
                <View style={a.viewAttitude}>
                  <Text style={a.nameAttitude}>Giá nhập</Text>
                  <Text>{data1.giavon} VND</Text>
                </View>
              </View>

              {/* Show thuộc tính còn lại của sản phẩm */}
              <View style={{ backgroundColor: "white", flexDirection: "row", paddingTop: 50, paddingLeft: 10, paddingBottom: 10, position: "relative" }}>
                <Text style={{ fontSize: 30 }}>Thuộc tính sản phẩm</Text>
              </View>
              <View style={{ flexDirection: "column" }}>
                <View style={a.viewAttitude}>
                  <Text style={a.nameAttitude}>Màu sắc</Text>
                  <Text>{data1.mausac}</Text>
                </View>
                <View style={a.viewAttitude}>
                  <Text style={a.nameAttitude}>Trọng lượng</Text>
                  <Text>{data1.trongluong}g</Text>
                </View>
                <View style={a.viewAttitude}>
                  <Text style={a.nameAttitude}>Mã sản phẩm</Text>
                  <Text>{data1.ma_sp}</Text>
                </View>
                <View style={a.viewAttitude}>
                  <Text style={a.nameAttitude}>Nhà sản xuất</Text>
                  <Text>{data1.nhasanxuat_info.map(data2 => (data2.tennsx))}</Text>
                </View>
                <View style={a.viewAttitude}>
                  <Text style={a.nameAttitude}>Mô tả</Text>
                  <Text>{data1.mota}</Text>
                </View>
                <View style={a.viewAttitude}>
                  <Text style={a.nameAttitude}>Bảo hành</Text>
                  <Text>{data1.baohanh} tháng</Text>
                </View>
              </View>
            </View>
          </View>

        ))}
      </ScrollView>


    );
  }
}
var a = StyleSheet.create({
  bao: {
    flexDirection: "row",
    borderBottomWidth: 1,
    marginBottom: 2,
    padding: 4
  },
  trai: {
    flex: 1,
    flexDirection: "row",
  },
  phai: {
    flex: 3
  },
  amount: {
    flex: 1,
    borderWidth: 1,
    borderColor: "gray",
    padding: 15
  },
  nameAttitude: {
    color: "gray"
  },
  viewAttitude: {
    borderBottomWidth: 1,
    padding: 10,
    marginTop: 5
  }
});


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    // backgroundColor: '#3498db',
    //position: 'absolute',
  },
  logo: {
    width: 50,
    height: 50,
  },
  floatingButton: {
    alignSelf: 'flex-end',
    bottom: 35,
    flex: 1,
    position: "absolute"
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});

const actions = [{
  text: 'Cập nhật',
  icon: require('../../images/ic_accessibility_white.png'),
  name: 'bt_accessibility',
  position: 1
}];