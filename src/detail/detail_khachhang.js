import React, { Component } from 'react';
import { Text, View, StyleSheet, ActivityIndicator, Image, ScrollView, TouchableOpacity, Alert } from 'react-native';
import { Header, Avatar, Icon } from 'react-native-elements';
import { toUnicode } from 'punycode';
import { FloatingAction } from 'react-native-floating-action';
import ActionButton from 'react-native-action-button';





export default class detail_khachhang extends Component {
  static navigationOptions = {
    title: 'Chi tiết khách hàng',
  };
  state = { idkh: 5 }
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: [],
      hello: this.props.navigation.state.params.idkh

    }
  }

  componentDidMount() {
    let data_send = {
      method: 'POST',
      body: JSON.stringify({
        id_kh: this.state.hello,
      }),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }
    fetch('http://192.168.1.215:3000/chitietkhachhang', data_send)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          data: responseJson,
          data_danhmuc: responseJson.danhmuc_info
        }, function () {

        });

      })
      .catch((error) => {
        console.error(error);
      });
  }
  render() {
    const { navigate } = this.props.navigation;

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }
    return (
      <ScrollView>
        <View>
          <Header
            leftComponent={
              <TouchableOpacity onPress={() =>
                this.props.navigation.openDrawer()}>
                <Image
                  style={styles.logo}
                  source={require('../../icons/ham.png')}
                />
              </TouchableOpacity>

            }
            centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
          />


          {/* <FloatingAction
            style={StyleSheet.floatingButton}
            actions={actions}
            onPressMain={
              (name) => {
                if (name == 'bt_accessibility') {
                  // Alert.alert("button video");
                  navigate('Upkhachhang',{idkh :this.state.hello})

                }
                console.log(`selected button: ${name}`);
              }
            }
          /> */}

          <ActionButton buttonColor="rgba(231,76,60,1)">
            <ActionButton.Item buttonColor='#9b59b6' title="Cập Nhật Thông Tin" onPress={() => { navigate('Upkhachhang',{idkh :this.state.hello})}}>
              <Icon name="md-create" style={styles.actionButtonIcon} />
            </ActionButton.Item>
          </ActionButton>

          {this.state.data.map(data1 => (

            <View style={{ flexDirection: "column" }}>
              <View style={a.viewAttitude}>
                <Text style={a.nameAttitude}>Tên khách hàng</Text>
                <Text>{data1.ten}</Text>
              </View>
              <View style={a.viewAttitude}>
                <Text style={a.nameAttitude}>Ngày sinh</Text>
                <Text>{data1.ngaysinh.substring(0, 10)}</Text>
              </View>
              <View style={a.viewAttitude}>
                <Text style={a.nameAttitude}>Giới tính</Text>
                <Text>{data1.gioitinh}</Text>
              </View>
              <View style={a.viewAttitude}>
                <Text style={a.nameAttitude}>Số điện thoại</Text>
                <Text>{data1.sdt} </Text>
              </View>
              <View style={a.viewAttitude}>
                <Text style={a.nameAttitude}>Địa chỉ</Text>
                <Text>{data1.diachi} </Text>
              </View>
              <View style={a.viewAttitude}>
                <Text style={a.nameAttitude}>Email</Text>
                <Text>{data1.email}</Text>
              </View>
            </View>

          ))}
        </View>

      </ScrollView>
    );
  }
}
var a = StyleSheet.create({
  bao: {
    flexDirection: "row",
    borderBottomWidth: 1,
    marginBottom: 2,
    padding: 4
  },
  trai: {
    flex: 1,
    flexDirection: "row",
  },
  phai: {
    flex: 3
  },
  amount: {
    flex: 1,
    borderWidth: 1,
    borderColor: "gray",
    padding: 15
  },
  nameAttitude: {
    color: "gray"
  },
  viewAttitude: {
    borderBottomWidth: 1,
    padding: 10,
    marginTop: 5
  }
});
const styles = StyleSheet.create({

  logo: {
    width: 50,
    height: 50,
  },
  floatingButton: {
    alignSelf: 'flex-end',
    bottom: 35,
    flex: 1,
    position: "absolute"

  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});

const actions = [{
  text: 'Cập nhật',
  icon: require('../../images/ic_accessibility_white.png'),
  name: 'bt_accessibility',
  position: 1
}];