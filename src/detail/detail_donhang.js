import React, { Component } from 'react';
import { Text, View, StyleSheet, ListView, Image, ScrollView,Alert } from 'react-native';
import { FloatingAction } from 'react-native-floating-action';
import ActionButton from 'react-native-action-button';
import { Header, Avatar, Icon } from 'react-native-elements';



export default class detail_donhang extends Component {
  static navigationOptions = {
    title: 'Chi tiết đơn hàng',
  };


  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: [],
      data_danhmuc: [],
      hello: this.props.navigation.state.params.iddh,
      trangthai: this.props.navigation.state.params.trangthai,
    }
  }

  xacnhandonhang() {
    if(this.state.trangthai == "Đã Huỷ"){
      Alert.alert("Đơn hàng đã bị hủy")
    }
   else if(this.state.trangthai == "Đã thanh toán"){
      Alert.alert("Đơn hàng này đã được xác nhận trước đó")
    }
    else{
      Alert.alert(
        'Xác nhận thanh toán đơn hàng',
        'Bạn đã chắc chắn chưa?',
      [
        { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: 'OK', onPress: () => fetch('http://192.168.1.215:3000/capnhat_dathanhtoandonhang', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              id_dh: this.state.hello,
            }),
          })
        },
      ],
      { cancelable: false }
    )
   
    }
   
  }

  Huydonhang() {
    if(this.state.trangthai == "Đã Huỷ"){
      Alert.alert("Đơn hàng này đã được hủy trước đó")
    }
    else{
      Alert.alert(
        'Xác nhận hủy đơn hàng',
        'Bạn đã chắc chắn chưa?',
      [
        { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: 'OK', onPress: () => fetch('http://192.168.1.215:3000/capnhat_huydonhang', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              id_dh: this.state.hello,
            }),
          })
        },
      ],
      { cancelable: false }
    )
   
    }
   
  }

  componentDidMount() {
    let data_send = {
      method: 'POST',
      body: JSON.stringify({
        id_dh: this.state.hello,
      }),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }
    fetch('http://192.168.1.215:3000/chitietdonhang_byid', data_send)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          data: responseJson
        }, function () {

        });

      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
     
      <ScrollView >
        <ActionButton buttonColor="rgba(231,76,60,1)">
          <ActionButton.Item buttonColor='#9b59b6' title="Xác nhận" onPress={() => this.xacnhandonhang()}>
            <Icon name="md-create" style={a.actionButtonIcon} />
          </ActionButton.Item>
          <ActionButton.Item buttonColor='#3498db' title="Hủy" onPress={() => this.Huydonhang()}>
            <Icon name="md-notifications-off" style={a.actionButtonIcon} />
          </ActionButton.Item>
        </ActionButton>
        {/* <FloatingAction
          style={StyleSheet.floatingButton}
          actions={actions}
          onPressItem={
            (name) => {
              if (name == 'bt_accessibility') {
                // Alert.alert("button video");
                navigate('Updonhang')

              }
              console.log(`selected button: ${name}`);
            }
          }
        /> */}
      



        {this.state.data.map(data1 => (
          <View key={data1.id_dh} style={{position:"relative"}}>

            <View style={{ backgroundColor: "white", flexDirection: "row", paddingTop: 50, paddingLeft: 10, paddingBottom: 10 }}>
              <Text style={{ fontSize: 20 }}>Thông Tin Sản Phẩm</Text>
            </View>
            <View style={{ height: 10, backgroundColor: "white" }}></View>
            <View style={a.viewAttitude}>
              <View style={a.atiitudeRow}>
                <Text style={{ fontSize: 30 }}>Tổng tiền</Text>
              </View>
              <View style={a.atiitudeRow}>
                <Text style={{ fontSize: 20, textAlign: "right" }}>{data1.tongtien}</Text>
                <Text style={{ color: "grey", textAlign: "right" }}>{data1.trangthai}</Text>
              </View>
            </View>
            <View style={{ height: 10, backgroundColor: "white" }}></View>
            <View>
              {data1.sanpham_info.map(data2 => (
                <View key={data2.ten_sp} style={{ flexDirection: "column", borderBottomWidth: 1 }}>
                  {/* Show thông tin sản phẩm */}
                  <View style={a.viewAttitude_3}>
                    <Text>{data2.ten_sp}</Text>
                    <Text style={a.nameAttitude}>{data2.ma_sp}</Text>
                  </View>
                  <View style={a.viewAttitude_2}>
                    <View style={a.atiitudeRow}>
                      <Text>Số lượng:{data1.soluong}</Text>
                    </View>
                  </View>
                  <View style={a.viewAttitude_2}>
                    <View style={a.atiitudeRow}>
                      <Text>Giá:{data2.giaban}</Text>
                    </View>
                  </View>
                  <View style={a.viewAttitude_2}>
                    <View style={a.atiitudeRow}>
                      <Text>Tiền hàng:{data2.giaban}</Text>
                    </View>
                    <View style={a.atiitudeRow}>
                      <Text>Tổng:{data1.tongtien}</Text>
                    </View>
                  </View>
                </View>
              ))}
            </View>
            <View style={{ backgroundColor: "white", flexDirection: "row", paddingTop: 50, paddingLeft: 10, paddingBottom: 10 }}>
              <Text style={{ fontSize: 20 }}>Thông tin khách hàng</Text>
            </View>
            <View style={a.viewAttitude}>
              <View style={a.atiitudeRow}>
                <Text style={a.nameAttitude}>Tên</Text>
              </View>
              <View style={a.atiitudeRow}>
                <Text style={a.rightText}>{data1.ten_KH}</Text>
              </View>
            </View>
            <View style={a.viewAttitude}>
              <View style={a.atiitudeRow}>
                <Text style={a.nameAttitude}>Điện thoại</Text>
              </View>
              <View style={a.atiitudeRow}>
                <Text style={a.rightText}>{data1.sdt}</Text>
              </View>
            </View>
            <View style={{flexDirection: "row", paddingTop: 50, paddingLeft: 10, paddingBottom: 10 }}>
              <Text style={{ fontSize: 20 }}>Thông tin vận chuyển</Text>
            </View>
            {data1.vanchuyen_info.map(data3 => (
              <View style={{position:"relative"}}>
                <View style={a.viewAttitude}>
                  <View style={a.atiitudeRow}>
                    <Text style={a.nameAttitude}>Loại đơn</Text>
                  </View>
                  <View style={a.atiitudeRow}>
                    <Text style={a.rightText}>{data1.loaidon}</Text>
                  </View>
                </View>
                <View style={a.viewAttitude}>
                  <View style={a.atiitudeRow}>
                    <Text style={a.nameAttitude}>Tên vận chuyển</Text>
                  </View>
                  <View style={a.atiitudeRow}>
                    <Text style={a.rightText}>{data3.tenvc}</Text>
                  </View>
                </View>
                <View style={a.viewAttitude}>
                  <View style={a.atiitudeRow}>
                    <Text style={a.nameAttitude}>Giá tiền vận chuyển</Text>
                  </View>
                  <View style={a.atiitudeRow}>
                    <Text style={a.rightText}>{data3.giatien}</Text>
                  </View>
                </View>
                <View style={a.viewAttitude}>
                  <View style={a.atiitudeRow}>
                    <Text style={a.nameAttitude}>Địa chỉ giao hàng</Text>
                  </View>
                  <View style={a.atiitudeRow}>
                    <Text style={a.rightText}>{data1.diachinhan}</Text>
                  </View>
                </View>


              </View>
            ))}

          </View>
        ))}
      </ScrollView>
      


    );
  }
}
var a = StyleSheet.create({
  bao: {
    flexDirection: "row",
    borderBottomWidth: 1,
    marginBottom: 2,
    padding: 4
  },
  trai: {
    flex: 1,
    flexDirection: "row",
  },
  phai: {
    flex: 3
  },
  amount: {
    flex: 1,
    borderWidth: 1,
    borderColor: "gray",
    padding: 15
  },
  nameAttitude: {
    color: "gray"
  },
  viewAttitude: {
    borderBottomWidth: 1,
    padding: 10,
    marginTop: 5,
    flexDirection: "row",
    position:"relative"
  },
  nameCustomer: {
    color: "gray",
    textAlign: "center",
    flex: 1,
    borderWidth: 1
  },
  atiitudeRow: {
    flex: 1,
    position:"relative"
  },
  rightText: {
    textAlign: "right"
  },
  viewAttitudeColumn: {
    padding: 10,
    marginTop: 5,
    flexDirection: "column"
  },
  viewAttitude_2: {
    padding: 10,
    marginTop: 5,
    flexDirection: "row"
  },
  viewAttitude_3: {
    padding: 10,
    marginTop: 5,

  },
  floatingButton: {
    alignSelf: 'flex-end',
    bottom: 35,
    flex: 1
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
    position:"absolute"
  },
});


const actions = [{
  text: 'Cập nhật',
  icon: require('../../images/ic_accessibility_white.png'),
  name: 'bt_accessibility',
  position: 1
}];