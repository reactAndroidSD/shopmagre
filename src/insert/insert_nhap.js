import React, { Component } from 'react';
import { Alert, TouchableOpacity, View, ActivityIndicator, StyleSheet, Text, FlatList, ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
export default class insert_nhap extends Component {

    static navigationOptions = {
        title: 'Thêm phiếu nhập',
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            id_sp:0,
            soluong: '',
            id_nsx: '',
            ghichu: '',
        }
    }

    componentDidMount1() {
        if (this.state.soluong == "") {
            Alert.alert("Vui lòng nhập số lượng");
        }
        else if (isNaN(this.state.soluong)) {
            Alert.alert("Vui lòng nhập số (số lượng)");
        }
        else if (this.state.id_sp == "") {
            Alert.alert("Vui lòng nhập sản phẩm");
        }
        else if (this.state.id_nsx == "") {
            Alert.alert("Vui lòng nhập nhà sản xuất");
        }
        else {
            Alert.alert(
                'Xác nhận',
                'Bạn đã chắc chắn chưa?',
                [
                    { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    {
                        text: 'OK', onPress: () => fetch('http://192.168.1.215:3000/themdonnhapkho', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                id_sp : parseInt(this.state.id_sp),
                                soluong: parseInt(this.state.soluong),
                                id_nsx: parseInt(this.state.id_nsx),
                                ghichu: this.state.ghichu,
                            }),
                        })
                    },
                ],
                { cancelable: false }
            )
        }

    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    {/* <Header
                        leftComponent={
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.openDrawer()}>
                                <Image
                                    style={styles.logo}
                                    source={require('../../icons/ham.png')}
                                />
                            </TouchableOpacity>

                        }
                        centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                    /> */}

                    <ScrollView>
                        <View>
                        <Text>Sản phẩm </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(id_sp) => this.setState({ id_sp })}


                            />
                            <Text>Số lượng: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(soluong) => this.setState({ soluong })}


                            />

                            <Text>Mã nhà sản xuất: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(id_nsx) => this.setState({ id_nsx })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />

                            <Text>Ghi chú: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(ghichu) => this.setState({ ghichu })}


                            />

                            <Button
                                title="ADD"
                                color="#ff0000"
                                onPress={() => this.componentDidMount1()}>

                            </Button>


                        </View>
                    </ScrollView>
                </View>
            )
        }
    }
}


// define your styles
const styles = StyleSheet.create({
    input: {
        marginBottom: 20,
        borderWidth: 1,
        paddingHorizontal: 10,

    },

});