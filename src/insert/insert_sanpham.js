import React, { Component } from 'react';
import { Alert, TouchableOpacity, View, ActivityIndicator, StyleSheet, Text, FlatList,  ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
export default class insert_sanpham extends Component {

    static navigationOptions = {
        title: 'Thêm sản phẩm',
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            ten_sp: '',
            anh: '',
            mavach: '',
            id_danhmuc: '',
            giavon: '',
            giaban: '',
            giabuon: '',
            soluong: '',
            mausac: '',
            trongluong: '',
            baohanh: '',
            id_nhasanxuat: '',
            mota: '',
        }
    }

    componentDidMount1() {
        if (this.state.ten_sp == "") {
            Alert.alert("Vui lòng nhập tên sản phẩm");
        }
        else if (this.state.id_danhmuc == "") {
            Alert.alert("Vui lòng nhập tên danh mục");
        }
        else if (this.state.mavach == "") {
            Alert.alert("Vui lòng nhập mã vạch");
        }
        else if (this.state.giavon == "") {
            Alert.alert("Vui lòng nhập giá vốn");
        }
        else if (isNaN(this.state.giavon)) {
            Alert.alert("Vui lòng nhập số (giá vốn)");
        }
        else if (isNaN(this.state.giaban)) {
            Alert.alert("Vui lòng nhập số ( giá bán )");
        }
        else if (isNaN(this.state.giabuon)) {
            Alert.alert("Vui lòng nhập số (giá buôn)");
        }
        else if (this.state.giaban == "") {
            Alert.alert("Vui lòng nhập giá bán");
        }
        else if (this.state.giabuon == "") {
            Alert.alert("Vui lòng nhập giá buôn");
        }
        else if (isNaN(this.state.soluong)) {
            Alert.alert("Vui lòng nhập số (số lượng)");
        }
        else if (this.state.soluong == "") {
            Alert.alert("Vui lòng nhập số lượng");
        }
        else if (this.state.mausac == "") {
            Alert.alert("Vui lòng nhập màu sắc");
        }
        else if (isNaN(this.state.trongluong)) {
            Alert.alert("Vui lòng nhập số (trọng lượng)");
        }
        else if (this.state.trongluong == "") {
            Alert.alert("Vui lòng nhập trọng lượng");
        }
        else if (isNaN(this.state.baohanh)) {
            Alert.alert("Vui lòng nhập số (bảo hành)");
        }
        else if (this.state.baohanh == "") {
            Alert.alert("Vui lòng nhập bảo hành");
        }
        else if (this.state.id_nhasanxuat == "") {
            Alert.alert("Vui lòng nhập ID nhà sản xuất");
        }
        else {
            Alert.alert(
                'Xác nhận',
                'Bạn đã chắc chắn chưa?',
                [
                    { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    {
                        text: 'OK', onPress: () => fetch('http://192.168.1.215:3000/themsanpham', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                ten_sp: this.state.ten_sp,
                                anh: this.state.anh,
                                mavach: this.state.mavach,
                                id_danhmuc: parseInt(this.state.id_danhmuc),
                                giavon: parseInt(this.state.giavon),
                                giaban: parseInt(this.state.giaban),
                                giabuon: parseInt(this.state.giabuon),
                                soluong: parseInt(this.state.soluong),
                                mausac: this.state.mausac,
                                trongluong: parseInt(this.state.trongluong),
                                baohanh: parseInt(this.state.baohanh),
                                id_nhasanxuat: parseInt(this.state.id_nhasanxuat),
                                mota: this.state.mota,
                            }),
                        })
                    },
                ],
                { cancelable: false }
            )
        }


    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    {/* <Header
                        leftComponent={
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.openDrawer()}>
                                <Image
                                    style={styles.logo}
                                    source={require('../../icons/ham.png')}
                                />
                            </TouchableOpacity>

                        }
                        centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                    /> */}

                    <ScrollView>
                        <View>

                            <Text>Tên sản phẩm: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(ten_sp) => this.setState({ ten_sp })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />

                            <Text>Ảnh: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(anh) => this.setState({ anh })}


                            />

                            <Text>Mã vạch: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(mavach) => this.setState({ mavach })}


                            />

                            <Text>ID danh mục: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(id_danhmuc) => this.setState({ id_danhmuc })}


                            />

                            <Text>Giá vốn: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(giavon) => this.setState({ giavon })}


                            />

                            <Text>Giá bán: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(giaban) => this.setState({ giaban })}


                            />

                            <Text>Giá buôn: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(giabuon) => this.setState({ giabuon })}


                            />

                            <Text>Số lượng: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(soluong) => this.setState({ soluong })}


                            />
                            <Text>Màu sắc: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(mausac) => this.setState({ mausac })}


                            />

                            <Text>Trọng lượng: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(trongluong) => this.setState({ trongluong })}


                            />

                            <Text>Bảo hành: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(baohanh) => this.setState({ baohanh })}


                            />

                            <Text>Nhà sản xuất: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(id_nhasanxuat) => this.setState({ id_nhasanxuat })}


                            />


                            <Text>Mô tả: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(mota) => this.setState({ mota })}


                            />


                            <Button
                                title="ADD"
                                color="#ff0000"
                                onPress={() => this.componentDidMount1()}>

                            </Button>


                        </View>
                    </ScrollView>
                </View>
            )
        }
    }
}

// define your styles
const styles = StyleSheet.create({
    input: {
        marginBottom: 20,
        borderWidth: 1,
        paddingHorizontal: 10,

    },

});