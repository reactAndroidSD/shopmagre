import React, { Component } from 'react';
import { Alert, TouchableOpacity, View, ActivityIndicator, StyleSheet, Text, FlatList, ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
export default class insert_danhmuc extends Component {
  static navigationOptions = {
    title: 'Thêm danh mục',
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: [],
      danhmuc: '',
      madanhmuc: ''
    }
  }

  componentDidMount1() {
    if (this.state.danhmuc == "") {
      Alert.alert("Vui lòng nhập tên danh mục");
    }
    else if (this.state.madanhmuc == "") {
      Alert.alert("Vui lòng nhập mã danh mục");
    }
    else {
      Alert.alert(
        'Xác nhận',
        'Bạn đã chắc chắn chưa?',
        [
          { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          {
            text: 'OK', onPress: () => fetch('http://192.168.1.215:3000/themdanhmuc', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                tendanhmuc: this.state.danhmuc,
                madanhmuc: this.state.madanhmuc,
              }),
            })
          },
        ],
        { cancelable: false }


      )
    }

  }


  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          {/* <Header
            leftComponent={
              <TouchableOpacity onPress={() =>
                this.props.navigation.openDrawer()}>
                <Image
                  style={styles.logo}
                  source={require('../../icons/ham.png')}
                />
              </TouchableOpacity>

            }
            centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
          /> */}

          <ScrollView>
            <View>
              <Text>Tên danh mục: </Text>
              <TextInput
                placeholder="..."
                placeholderTextColor='#85adad'
                returnKeyType="next"
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.input}
                onChangeText={(danhmuc) => this.setState({ danhmuc })}


              />
              <Text>Mã danh mục: </Text>
              <TextInput
                placeholder="..."
                placeholderTextColor='#85adad'
                // secureTextEntry
                onChangeText={(madanhmuc) => this.setState({ madanhmuc })}
                returnKeyType="go"
                autoCapitalize="none"
                style={styles.input}
                autoCorrect={false}
              />


              <Button
                title="ADD"
                color="#ff0000"
                onPress={() => this.componentDidMount1()}>

              </Button>


            </View>
          </ScrollView>
        </View>
      )
    }
  }
}


// define your styles
const styles = StyleSheet.create({
  input: {
    marginBottom: 20,
    borderWidth: 1,
    paddingHorizontal: 10,

  },

});