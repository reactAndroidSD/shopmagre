import React, { Component } from 'react';
import { Alert, TouchableOpacity, View, ActivityIndicator, StyleSheet, Text, FlatList, ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
import {Select, Option} from "react-native-chooser";
export default class insert_donhang extends Component {

    static navigationOptions = {
        title: 'Thêm đơn hàng',
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            ten_KH: '',
            id_sp: '',
            loaidon: '',
            soluong: '',
            vanchuyen: '',
            sdt: '',
            diachinhan: '',
            // trangthai: '',
            ghichu:'',
        }
    }

    onSelect1(value, label1) {
        this.setState({ loaidon: value});
    }

    componentDidMount1() {
        if (this.state.ten_KH == "") {
            Alert.alert("Vui lòng nhập tên tên khách hàng");
        }
        else if (this.state.id_sp == "") {
            Alert.alert("Vui lòng nhập mã sản phẩm");
        }
        else if (this.state.loaidon == "") {
            Alert.alert("Vui lòng nhập loại đơn");
        }
        else if (isNaN(this.state.soluong)) {
            Alert.alert("Vui lòng nhập dạng số ( số lượng)");
        }
        else if (this.state.soluong == "") {
            Alert.alert("Vui lòng nhập số lượng");
        }
        else if (this.state.sdt == "") {
            Alert.alert("Vui lòng nhập số điện thoại");
        }
        else {
            Alert.alert(
                'Xác nhận',
                'Bạn đã chắc chắn chưa?',
                [
                  { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                  {
                    text: 'OK', onPress: () => fetch('http://192.168.1.215:3000/themdonhang', {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            ten_KH: this.state.ten_KH,
                            id_sp: parseInt(this.state.id_sp),
                            loaidon: this.state.loaidon,
                            soluong: parseInt(this.state.soluong),
                            vanchuyen: parseInt(this.state.vanchuyen),
                            sdt: this.state.sdt,
                            diachinhan: this.state.diachinhan,
                            // trangthai: this.state.trangthai,
                            ghichu : this.state.ghichu
                        }),
                    })
                  },
                ],
                { cancelable: false }
        
        
              )
            
        }

    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    {/* <Header
                        leftComponent={
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.openDrawer()}>
                                <Image
                                    style={styles.logo}
                                    source={require('../../icons/ham.png')}
                                />
                            </TouchableOpacity>

                        }
                        centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                    /> */}

                    <ScrollView>
                        <View>
                            <Text>Tên khách hàng: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                style={styles.input}
                                onChangeText={(ten_KH) => this.setState({ ten_KH })}


                            />

                            <Text>Sản phẩm: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(id_sp) => this.setState({ id_sp })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                style={styles.input}
                                autoCorrect={false}
                            />
                            <Text>Loại đơn: </Text>
                            <Select
                                onSelect={this.onSelect1.bind(this)}
                                defaultText={this.state.loaidon}
                                style={{ borderWidth: 1, borderColor: "green" }}
                                textStyle={{}}
                                backdropStyle={{ backgroundColor: "#d3d5d6" }}
                                optionListStyle={{ backgroundColor: "#F5FCFF" }}
                            >
                                <Option value="online">online</Option>
                                <Option value="offline">offline</Option>

                            </Select>

                            <Text>Số lượng: </Text>
                            <TextInput
                                style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(soluong) => this.setState({ soluong })}


                            />

                            <Text>Vận chuyển: </Text>
                            <TextInput
                                style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(vanchuyen) => this.setState({ vanchuyen })}


                            />

                            <Text>Số điện thoại: </Text>
                            <TextInput
                                style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType='phone-pad'
                                onChangeText={(sdt) =>  this.setState({ sdt })}


                            />

                            <Text>Địa chỉ nhận: </Text>
                            <TextInput
                                style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(diachinhan) => this.setState({ diachinhan })}


                            />

                            {/* <Text>Trạng thái: </Text>
                            <TextInput
                                style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(trangthai) => this.setState({ trangthai })}


                            /> */}

                            <Text>Ghi chú </Text>
                            <TextInput
                                style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(ghichu) => this.setState({ ghichu })}


                            />




                            <Button
                                title="ADD"
                                color="#ff0000"
                                onPress={() => this.componentDidMount1()}>

                            </Button>


                        </View>
                    </ScrollView>
                </View>
            )
        }
    }
}

// define your styles
const styles = StyleSheet.create({
    input: {
        marginBottom: 20,
        borderWidth: 1,
        paddingHorizontal: 10,

    },

});