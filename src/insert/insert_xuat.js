import React, { Component } from 'react';
import { Alert, TouchableOpacity, View, ActivityIndicator, StyleSheet, Text, FlatList, ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
export default class insert_xuat extends Component {

    static navigationOptions = {
        title: 'Thêm phiếu xuất',
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            ghichu: '',
            id_sp: 0,
            soluong: 0,
            tenkh: '',
            sdt: '',
            diachi: '',
        }
    }

    componentDidMount1() {
        // if (this.state.ghichu == "") {
        //     Alert.alert("Vui lòng nhập ghi chú");
        // }
        // else 
        if (this.state.id_sp == "") {
            Alert.alert("Vui lòng nhập ID sản phẩm");
        }
        else if (isNaN(this.state.soluong)) {
            Alert.alert("Vui lòng nhập số (số lượng)");
        }
        else if (this.state.soluong == "") {
            Alert.alert("Vui lòng số lượng");
        }
        else if (this.state.tenkh == "") {
            Alert.alert("Vui lòng nhập tên khách hàng");
        }
        else if (this.state.sdt == "") {
            Alert.alert("Vui lòng nhập số điện thoại");
        }
        else if (this.state.diachi == "") {
            Alert.alert("Vui lòng nhập địa chỉ");
        }
        else {
            Alert.alert(
                'Xác nhận',
                'Bạn đã chắc chắn chưa?',
                [
                    { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    {
                        text: 'OK', onPress: () => fetch('http://192.168.1.215:3000/themdonxuatkho', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({

                                ghichu: this.state.ghichu,
                                id_sp: parseInt(this.state.id_sp),
                                soluong: parseInt(this.state.soluong),
                                tenkh: this.state.tenkh,
                                sdt: this.state.sdt,
                                diachi: this.state.diachi,
                            }),
                        })
                    },
                ],
                { cancelable: false }
            )

        }


    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>


                    <ScrollView>
                        <View>
                            <Text>Sản phẩm: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(id_sp) => this.setState({ id_sp })}


                            />

                            <Text>Số lượng: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(soluong) => this.setState({ soluong })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />

                            <Text>Tên Khách hàng: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(tenkh) => this.setState({ tenkh })}


                            />

                            <Text>Số điện thoại: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType='phone-pad'
                                onChangeText={(sdt) => this.setState({ sdt })}


                            />

                            <Text>Địa chỉ: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(diachi) => this.setState({ diachi })}


                            />

                            <Text>Ghi chú: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(ghichu) => this.setState({ ghichu })}


                            />

                            <Button
                                title="ADD"
                                color="#ff0000"
                                onPress={() => this.componentDidMount1()}>

                            </Button>


                        </View>
                    </ScrollView>
                </View>
            )
        }
    }
}

// define your styles
const styles = StyleSheet.create({
    input: {
        marginBottom: 20,
        borderWidth: 1,
        paddingHorizontal: 10,

    },

});