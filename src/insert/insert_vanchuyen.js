import React, { Component } from 'react';
import { Alert, TouchableOpacity, View, ActivityIndicator, StyleSheet, Text, FlatList,  ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
export default class insert_vanchuyen extends Component {

    static navigationOptions = {
        title: 'Thêm vận chuyển',
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            tenvc: '',
            giatien: 1000,
            ghichu: ''
        }
    }

    componentDidMount1() {
        if (this.state.tenvc == "") {
            Alert.alert("Vui lòng nhập đơn vị vận chuyển");
        }
        else if (isNaN(this.state.giatien)) {
            Alert.alert("Vui lòng nhập số (giá tiền)");
        }
        else if (this.state.giatien == "") {
            Alert.alert("Vui lòng nhập giá tiền");
        }
        else {
            Alert.alert(
                'Xác nhận',
                'Bạn đã chắc chắn chưa?',
                [
                    { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    {
                        text: 'OK', onPress: () => fetch('http://192.168.1.215:3000/themvanchuyen', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                tenvc: this.state.tenvc,
                                giatien: parseInt(this.state.giatien),
                                ghichu: this.state.ghichu
                            }),
                        })
                    },
                ],
                { cancelable: false }
            )
        }

    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    {/* <Header
                        leftComponent={
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.openDrawer()}>
                                <Image
                                    style={styles.logo}
                                    source={require('../../icons/ham.png')}
                                />
                            </TouchableOpacity>

                        }
                        centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                    /> */}

                    <ScrollView>
                        <View>
                            <Text>Tên đơn vị vận chuyển: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(tenvc) => this.setState({ tenvc })}


                            />
                            <Text>Giá tiền: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(giatien) => this.setState({ giatien })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />

                            <Text>Ghi chú: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(ghichu) => this.setState({ ghichu })}


                            />




                            <Button
                                title="ADD"
                                color="#ff0000"
                                onPress={() => this.componentDidMount1()}>

                            </Button>


                        </View>
                    </ScrollView>
                </View>
            )
        }
    }
}

// define your styles
const styles = StyleSheet.create({
    input: {
        marginBottom: 20,
        borderWidth: 1,
        paddingHorizontal: 10,

    },

});