//import liraries
import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, StatusBar, TouchableOpacity, Image } from 'react-native';
import { createDrawerNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import ds_hoadon from '../page/ds_hoadon'
import ds_donhang from '../page/ds_donhang'
import insert_donhang from '../insert/insert_donhang'
import { Header, Avatar, Icon } from 'react-native-elements';
import detail_donhang from '../detail/detail_donhang'
import up_donhang from '../update/up_donhang'




// create a component
class MyDrawer3 extends Component {
    static navigationOptions = {
        title: 'Quản lý đơn hàng',
    };
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Header
                    leftComponent={
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.openDrawer()}>
                            <Image
                                style={styles.logo}
                                source={require('../../icons/ham.png')}
                            />
                        </TouchableOpacity>

                    }
                    centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                />



                <View style={styles.buttonContainer1}>
                    <TouchableOpacity style={{ flex: 1 }}
                        onPress={() =>
                            this.props.navigation.navigate('ListDH')}
                    >
                        <Text style={styles.textStyle}>Danh Sách Đơn Hàng</Text>

                    </TouchableOpacity>
                </View>

                <View style={styles.buttonContainer2}>
                    <TouchableOpacity style={{ flex: 1 }}
                        onPress={() =>
                            this.props.navigation.navigate('ListHD')}
                    >
                        <Text style={styles.textStyle}>Danh Sách Hóa Đơn</Text>

                    </TouchableOpacity>
                </View>

                <View style={styles.buttonContainer3}>
                    <TouchableOpacity style={{ flex: 1 }}
                        onPress={() =>
                            this.props.navigation.navigate('Add')}>

                        <Text style={styles.textStyle} >Thêm Mới</Text>
                    </TouchableOpacity>
                </View>



            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2c3e50',
        flexDirection: 'column',
        width: '100%',
        height: '100%',
    },
    logo: {
        width: 50,
        height: 50,
    },
    buttonContainer1: {
        flex: 1,
        // flexDirection: 'column',
        width: '100%',
        height: '100%',
        backgroundColor: '#00bfff',
        alignItems: 'center',
        justifyContent: 'center',

    },
    buttonContainer2: {
        flex: 1,
        // flexDirection: 'column',
        width: '100%',
        height: '100%',
        backgroundColor: '#0080ff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonContainer3: {
        flex: 1,
        // flexDirection: 'column',
        width: '100%',
        height: '100%',
        backgroundColor: '#00bfff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textStyle: {
        flex: 1,
        color: "white",
        fontSize: 30,
        textAlignVertical: 'center'

    }
});

const AppNavigator = createStackNavigator({
    Main: {
        screen: MyDrawer3
    },
    ListDH: {
        screen: ds_donhang
    },
    ListHD: {
        screen: ds_hoadon
    },
    Add: {
        screen: insert_donhang
    },
    Detaildonhang: {
        screen: detail_donhang
    },
    Updonhang: {
        screen: up_donhang
    }
},
    {
        initialRouteName: "Main"
    }
);

export default createAppContainer(AppNavigator);
