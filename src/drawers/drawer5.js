//import liraries
import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, StatusBar, TouchableOpacity, Image } from 'react-native';
import { createDrawerNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import ds_danhmuc from '../page/ds_danhmuc'
import insert_danhmuc from '../insert/insert_danhmuc'
import { Header, Avatar, Icon } from 'react-native-elements';
import up_danhmuc from '../update/up_danhmuc'



// create a component
class MyDrawer5 extends Component {
    static navigationOptions = {
        title: 'Quản lý danh mục',
    };
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Header
                    leftComponent={
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.openDrawer()}>
                            <Image
                                style={styles.logo}
                                source={require('../../icons/ham.png')}
                            />
                        </TouchableOpacity>

                    }
                    centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                />



                <View style={styles.buttonContainer1}>
                    <TouchableOpacity style={{ flex: 1 }}
                        onPress={() =>
                            this.props.navigation.navigate('ListDM')}
                    >
                        <Text style={styles.textStyle}>Danh Danh Mục</Text>

                    </TouchableOpacity>
                </View>

                <View style={styles.buttonContainer2}>
                    <TouchableOpacity style={{ flex: 1 }}
                        onPress={() =>
                            this.props.navigation.navigate('AddDM')}>

                        <Text style={styles.textStyle} >Thêm Danh Mục</Text>
                    </TouchableOpacity>
                </View>



            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2c3e50',
        flexDirection: 'column',
        width: '100%',
        height: '100%',
    },
    logo: {
        width: 50,
        height: 50,
    },
    buttonContainer1: {
        flex: 1,
        // flexDirection: 'column',
        width: '100%',
        height: '100%',
        backgroundColor: '#00bfff',
        alignItems: 'center',
        justifyContent: 'center',

    },
    buttonContainer2: {
        flex: 1,
        // flexDirection: 'column',
        width: '100%',
        height: '100%',
        backgroundColor: '#0080ff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textStyle: {
        flex: 1,
        color: "white",
        fontSize: 30,
        textAlignVertical: 'center'

    }
});

const AppNavigator = createStackNavigator({
    Main: {
        screen: MyDrawer5
    },
    ListDM: {
        screen: ds_danhmuc
    },
    AddDM: {
        screen: insert_danhmuc
    },
    Updanhmuc: {
        screen: up_danhmuc
    }
},
    {
        initialRouteName: "Main"
    }
);

export default createAppContainer(AppNavigator);
