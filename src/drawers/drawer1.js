//import liraries
import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, StatusBar, TouchableOpacity, Image } from 'react-native';
import { createDrawerNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import ds_sanpham from '../page/ds_sanpham'
import insert_sanpham from '../insert/insert_sanpham'
import detail_sanpham from '../detail/detail_sanpham'
import { Header, Avatar, Icon } from 'react-native-elements';
import up_sanpham from '../update/up_sanpham'


// create a component
class MyDrawer1 extends Component {
    static navigationOptions = {
        title: 'Quản lý sản phẩm',
    };
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Header
                    leftComponent={
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.openDrawer()}>
                            <Image
                                style={styles.logo}
                                source={require('../../icons/ham.png')}
                            />
                        </TouchableOpacity>

                    }
                    centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                />

                <View style={styles.buttonContainer1}>
                    <TouchableOpacity style={{ flex: 1 }}
                        onPress={() =>
                            this.props.navigation.navigate('Danh sách sản phẩm')}
                    >
                        <Text style={styles.textStyle}>Danh Sách Sản Phẩm</Text>

                    </TouchableOpacity>
                </View>

                <View style={styles.buttonContainer2}>
                    <TouchableOpacity style={{ flex: 1 }}
                        onPress={() =>
                            this.props.navigation.navigate('Add')}>

                        <Text style={styles.textStyle} >Thêm Mới Sản Phẩm</Text>
                    </TouchableOpacity>
                </View>



            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2c3e50',
        flexDirection: 'column',
        width: '100%',
        height: '100%',
    },
    logo: {
        width: 50,
        height: 50,
    },
    buttonContainer1: {
        flex: 1,
        // flexDirection: 'column',
        width: '100%',
        height: '100%',
        backgroundColor: '#00bfff',
        alignItems: 'center',
        justifyContent: 'center',

    },
    buttonContainer2: {
        flex: 1,
        // flexDirection: 'column',
        width: '100%',
        height: '100%',
        backgroundColor: '#0080ff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textStyle: {
        flex: 1,
        color: "white",
        fontSize: 30,
        textAlignVertical: 'center'

    }
});

const AppNavigator = createStackNavigator({
    Main: {
        screen: MyDrawer1
    },
    "Danh sách sản phẩm": {
        screen: ds_sanpham
    },
    Add: {
        screen: insert_sanpham
    },
    Detailsp:{
        screen:detail_sanpham
    },
    Upsanpham:{
        screen: up_sanpham
    }
},
    {
        initialRouteName: "Main"
    }
);

export default createAppContainer(AppNavigator);
