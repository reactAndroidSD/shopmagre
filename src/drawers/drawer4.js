//import liraries
import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, StatusBar, TouchableOpacity, Image } from 'react-native';
import { createDrawerNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import ds_xuat from '../page/ds_xuat'
import ds_nhap from '../page/ds_nhap'
import insert_xuat from '../insert/insert_xuat'
import insert_nhap from '../insert/insert_nhap'
import { Header, Avatar, Icon } from 'react-native-elements';
import detail_donnhap_xuat from '../detail/detail_donnhap_xuat'
import up_donkho from '../update/up_donkho'





// create a component
class MyDrawer4 extends Component {
    static navigationOptions = {
        title: 'Quản lý kho hàng',
    };
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>

                <Header
                    leftComponent={
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.openDrawer()}>
                            <Image
                                style={styles.logo}
                                source={require('../../icons/ham.png')}
                            />
                        </TouchableOpacity>

                    }
                    centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                />



                <View style={styles.buttonContainer1}>
                    <TouchableOpacity style={{ flex: 1 }}
                        onPress={() =>
                            this.props.navigation.navigate('ListXuat')}
                    >
                        <Text style={styles.textStyle}>Danh Sách Phiếu Xuất</Text>

                    </TouchableOpacity>
                </View>

                <View style={styles.buttonContainer2}>
                    <TouchableOpacity style={{ flex: 1 }}
                        onPress={() =>
                            this.props.navigation.navigate('ListNhap')}
                    >
                        <Text style={styles.textStyle}>Danh Sách Phiếu Nhập</Text>

                    </TouchableOpacity>
                </View>

                <View style={styles.buttonContainer3}>
                    <TouchableOpacity style={{ flex: 1 }}
                        onPress={() =>
                            this.props.navigation.navigate('AddXuat')}>

                        <Text style={styles.textStyle} >Thêm Phiếu Xuất</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.buttonContainer4}>
                    <TouchableOpacity style={{ flex: 1 }}
                        onPress={() =>
                            this.props.navigation.navigate('AddNhap')}>

                        <Text style={styles.textStyle} >Thêm Phiếu Nhập</Text>
                    </TouchableOpacity>
                </View>



            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2c3e50',
        flexDirection: 'column',
        width: '100%',
        height: '100%',
    },
    logo: {
        width: 50,
        height: 50,
    },
    buttonContainer1: {
        flex: 1,
        // flexDirection: 'column',
        width: '100%',
        height: '100%',
        backgroundColor: '#00bfff',
        alignItems: 'center',
        justifyContent: 'center',

    },
    buttonContainer2: {
        flex: 1,
        // flexDirection: 'column',
        width: '100%',
        height: '100%',
        backgroundColor: '#0080ff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonContainer3: {
        flex: 1,
        // flexDirection: 'column',
        width: '100%',
        height: '100%',
        backgroundColor: '#00bfff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonContainer4: {
        flex: 1,
        // flexDirection: 'column',
        width: '100%',
        height: '100%',
        backgroundColor: '#0080ff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textStyle: {
        flex: 1,
        color: "white",
        fontSize: 30,
        textAlignVertical: 'center'

    }
});

const AppNavigator = createStackNavigator({
    Main: {
        screen: MyDrawer4
    },
    ListXuat: {
        screen: ds_xuat
    },
    ListNhap: {
        screen: ds_nhap
    },
    AddXuat: {
        screen: insert_xuat
    },
    AddNhap: {
        screen: insert_nhap
    },
    Detailkho: {
        screen: detail_donnhap_xuat
    },
    Updonkho: {
        screen: up_donkho
    }
},
    {
        initialRouteName: "Main"
    }
);

export default createAppContainer(AppNavigator);
