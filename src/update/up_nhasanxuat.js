import React, { Component } from 'react';
import { Alert, TouchableOpacity, View, ActivityIndicator, StyleSheet, Text, FlatList, ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
export default class insert_nsx extends Component {
    static navigationOptions = {
        title: 'Cập nhật Nhà Sản Xuất',
    };
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            tennsx: '',
            sdt: 0,
            diachi: '',
            email: ''
        }
    }

    componentDidMount1() {
        if (this.state.tennsx == "") {
            Alert.alert("Vui lòng nhập tên nhà sản xuất");
        }
        else if (this.state.sdt == "") {
            Alert.alert("Vui lòng nhập số điện thoại");
        }
        else if (this.state.diachi == "") {
            Alert.alert("Vui lòng nhập địa chỉ");
        }
        else if (this.state.email == "") {
            Alert.alert("Vui lòng nhập email");
        }
        else {
            Alert.alert(
                'Xác nhận',
                'Bạn đã chắc chắn chưa?',
                [
                    { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    {
                        text: 'OK', onPress: () => fetch('http://192.168.1.215:3000/themnhasanxuat', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                tennsx: this.state.tennsx,
                                sdt: this.state.sdt,
                                diachi: this.state.diachi,
                                email: this.state.email
                            }),
                        })
                    },
                ],
                { cancelable: false }
            )
        }



    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    {/* <Header
                        leftComponent={
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.openDrawer()}>
                                <Image
                                    style={styles.logo}
                                    source={require('../../icons/ham.png')}
                                />
                            </TouchableOpacity>

                        }
                        centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                    /> */}

                    <ScrollView>
                        <View>
                            <Text>Tên nhà sản xuất: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(tennsx) => this.setState({ tennsx })}


                            />
                            <Text>Số điện thoại NSX: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(sdt) => this.setState({ sdt })}
                                returnKeyType="go"
                                keyboardType='phone-pad'
                                autoCapitalize="none"
                                autoCorrect={false}
                            />

                            <Text>Địa chỉ: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(diachi) => this.setState({ diachi })}


                            />

                            <Text>Địa chỉ email: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(email) => this.setState({ email })}


                            />


                            <Button
                                title="ADD"
                                color="#ff0000"
                                onPress={() => this.componentDidMount1()}>

                            </Button>


                        </View>
                    </ScrollView>
                </View>
            )
        }
    }
}

// define your styles
const styles = StyleSheet.create({
    input: {
        marginBottom: 20,
        borderWidth: 1,
        paddingHorizontal: 10,

    },

});