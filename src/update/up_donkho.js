import React, { Component } from 'react';
import { Alert, TouchableOpacity, View, ActivityIndicator, StyleSheet, Text, FlatList, ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
export default class insert_donkho extends Component {
    static navigationOptions = {
        title: 'Cập nhật đơn kho',
    };
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            id_sp: '',
            soluong: '',
            id_nsx: '',
            tenkh: '',
            sdt: '',
            diachi: '',
            ghichu: '',

        }
    }

    componentDidMount1() {
        if (this.state.soluong == "") {
            Alert.alert("Vui lòng nhập số lượng");
        }
        else if (this.state.id_sp == "") {
            Alert.alert("Vui lòng nhập id sản phẩm");
        }
        else if (this.state.id_nsx == "") {
            Alert.alert("Vui lòng nhập nhà sản xuất");
        }
        else if (this.state.tenkh == "") {
            Alert.alert("Vui lòng nhập tên khách hàng");
        }
        else if (this.state.sdt == "") {
            Alert.alert("Vui lòng nhập số điện thoại");
        }
        else if (this.state.diachi == "") {
            Alert.alert("Vui lòng nhập địa chỉ");
        }
        else if (this.state.ghichu == "") {
            Alert.alert("Vui lòng nhập ghi chú");
        }
        else {
            Alert.alert(
                'Xác nhận',
                'Bạn đã chắc chắn chưa?',
                [
                    { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    {
                        text: 'OK', onPress: () => fetch('http://192.168.1.215:3000/themnhasanxuat', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                soluong: parseInt(this.state.soluong),
                                id_nsx: parseInt(this.state.id_nsx),
                                ghichu: this.state.ghichu,
                            }),
                        })
                    },
                ],
                { cancelable: false }
            )
        }

    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>


                    <ScrollView>
                        <View>
                            <Text>Số lượng: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(soluong) => this.setState({ soluong })}


                            />

                            <Text>Mã nhà sản xuất: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(id_nsx) => this.setState({ id_nsx })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />

                            <Text>Sản phẩm: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(id_sp) => this.setState({ id_sp })}
                            />

                            <Text>Tên khách hàng: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(tenkh) => this.setState({ tenkh })}
                            />

                            <Text>Số điện thoại: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType='phone-pad'
                                onChangeText={(sdt) => this.setState({ sdt })}
                            />

                            <Text>Địa chỉ: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(diachi) => this.setState({ diachi })}
                            />

                            <Text>Ghi chú: </Text>
                            <TextInput style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(ghichu) => this.setState({ ghichu })}
                            />


                            
                            <Button
                                title="ADD"
                                color="#ff0000"
                                onPress={() => this.componentDidMount1()}>

                            </Button>


                        </View>
                    </ScrollView>
                </View>
            )
        }
    }
}


// define your styles
const styles = StyleSheet.create({
    input: {
        marginBottom: 20,
        borderWidth: 1,
        paddingHorizontal: 10,

    },

});