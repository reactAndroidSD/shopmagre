import React, { Component } from 'react';
import { Select, Option } from "react-native-chooser";
import { Alert, TouchableOpacity, View, ActivityIndicator, StyleSheet, Text, FlatList, ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
import DatePicker from 'react-native-datepicker'

export default class insert_khachhang extends Component {
    static navigationOptions = {
        title: 'Cập nhật thông tin khách hàng',
    };
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            ten: '',
            ngaysinh: "2016-05-15",
            gioitinh: "",
            sdt: "",
            diachi: "",
            email: "",
            result: this.props.navigation.state.params.idkh

        }
    }

    componentDidMount() {
        let data_send2 = {
            method: 'POST',
            body: JSON.stringify({
                id_kh: this.state.result,
            }),
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        }
        fetch('http://192.168.1.215:3000/chitietkhachhang', data_send2)
            .then((response) => response.json())
            .then((responseJson) => {
                responseJson.map(data1 => (
                    this.setState({
                        ten: data1.ten,
                        ngaysinh: data1.ngaysinh,
                        gioitinh: data1.gioitinh,
                        sdt: data1.sdt,
                        diachi: data1.diachi,
                        email: data1.email,
                    })
                )),
                    this.setState({
                        // isLoading: false,
                        data: responseJson
                    }, function () {

                    });

            })
            .catch((error) => {
                console.error(error);
            });
    }

    onSelect(value, label) {
        this.setState({ gioitinh: value });
    }

    componentDidMount1() {
        if (this.state.ten == "") {
            Alert.alert("Vui lòng nhập tên khách hàng");
        }
        else if (this.state.ngaysinh == "") {
            Alert.alert("Vui lòng nhập ngày sinh");
        }
        else if (this.state.gioitinh == "") {
            Alert.alert("Vui lòng nhập giới tính");
        }
        else if (this.state.sdt == "") {
            Alert.alert("Vui lòng nhập số điện thoại");
        }
        else if (this.state.diachi == "") {
            Alert.alert("Vui lòng nhập địa chỉ");
        }
        else if (this.state.email == "") {
            Alert.alert("Vui lòng nhập email");
        }
        else {
            Alert.alert(
                'Xác nhận',
                'Bạn đã chắc chắn chưa?',
                [
                    { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    {
                        text: 'OK', onPress: () => fetch('http://192.168.1.215:3000/capnhatkhachhang', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                id_kh: this.state.result,
                                ten: this.state.ten,
                                ngaysinh: this.state.ngaysinh,
                                gioitinh: this.state.gioitinh,
                                sdt: this.state.sdt,
                                diachi: this.state.diachi,
                                email: this.state.email,
                            }),
                        })
                    },
                ],
                { cancelable: false }


            )
        }

    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>


                    <ScrollView>
                        {this.state.data.map(data1 => (


                            <View key={data1.id_kh} >
                                <Text>Tên khách hàng: </Text>
                                <TextInput style={styles.input}
                                    placeholder={data1.ten}
                                    placeholderTextColor='#85adad'
                                    returnKeyType="next"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    onChangeText={(ten) => this.setState({ ten })}


                                />
                                <Text>Ngày sinh: </Text>
                                <DatePicker
                                    style={{ width: 200 }}
                                    ngaysinh={this.state.ngaysinh}
                                    mode="date"
                                    placeholder="select date"
                                    format="YYYY-MM-DD"
                                    minDate="1990-05-01"
                                    maxDate="2090-06-01"
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    customStyles={{
                                        dateIcon: {
                                            position: 'absolute',
                                            left: 0,
                                            top: 4,
                                            marginLeft: 0
                                        },
                                        dateInput: {
                                            marginLeft: 36
                                        }
                                    }}
                                    onDateChange={(ngaysinh) => { this.setState({ ngaysinh: ngaysinh }) }}
                                />

                                <Text>Giới tính: </Text>
                                <Select
                                    onSelect={this.onSelect.bind(this)}
                                    defaultText={this.state.gioitinh}
                                    style={{ borderWidth: 1, borderColor: "green" }}
                                    textStyle={{}}
                                    backdropStyle={{ backgroundColor: "#d3d5d6" }}
                                    optionListStyle={{ backgroundColor: "#F5FCFF" }}
                                >
                                    <Option value="nam">nam</Option>
                                    <Option value="nữ">nữ</Option>

                                </Select>

                                <Text>Số điện thoại: </Text>
                                <TextInput style={styles.input}
                                    placeholder={data1.sdt}
                                    keyboardType='phone-pad'
                                    placeholderTextColor='#85adad'
                                    // secureTextEntry
                                    onChangeText={(sdt) => this.setState({ sdt })}
                                    returnKeyType="go"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                />

                                <Text>Địa chỉ: </Text>
                                <TextInput style={styles.input}
                                    placeholder={data1.diachi}
                                    placeholderTextColor='#85adad'
                                    // secureTextEntry
                                    onChangeText={(diachi) => this.setState({ diachi })}
                                    returnKeyType="go"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                />

                                <Text>Email: </Text>
                                <TextInput style={styles.input}
                                    placeholder={data1.email}
                                    placeholderTextColor='#85adad'
                                    // secureTextEntry
                                    onChangeText={(email) => this.setState({ email })}
                                    returnKeyType="go"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                />


                                <Button
                                    title="THAY ĐỔI"
                                    color="#ff0000"
                                    onPress={() => this.componentDidMount1()}>

                                </Button>


                            </View>
                        ))}
                    </ScrollView>
                </View>
            )
        }
    }
}

// define your styles
const styles = StyleSheet.create({
    input: {
        marginBottom: 20,
        borderWidth: 1,
        paddingHorizontal: 10,

    },

});