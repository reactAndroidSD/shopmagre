//import liraries
import React, { Component } from 'react';
import { TouchableOpacity, View, ActivityIndicator, StyleSheet, Text, FlatList, Alert, ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';

// create a component
class up_danhmuc extends Component {
    static navigationOptions = {
        title: 'Cập nhật danh mục',
    };
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            danhmuc: '',
            madanhmuc: ''
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View>
                        <Text>Tên danh mục: </Text>
                        <TextInput
                            placeholder="..."
                            placeholderTextColor='#85adad'
                            returnKeyType="next"
                            autoCapitalize="none"
                            autoCorrect={false}
                            style={styles.input}
                            onChangeText={(danhmuc) => this.setState({ danhmuc })}


                        />
                        <Text>Mã danh mục: </Text>
                        <TextInput
                            placeholder="..."
                            placeholderTextColor='#85adad'
                            // secureTextEntry
                            onChangeText={(madanhmuc) => this.setState({ madanhmuc })}
                            returnKeyType="go"
                            autoCapitalize="none"
                            style={styles.input}
                            autoCorrect={false}
                        />


                        <Button
                            title="ADD"
                            color="#ff0000"
                            onPress={() => this.componentDidMount1()}>

                        </Button>


                    </View>
                </ScrollView>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    input: {
      marginBottom: 20,
      borderWidth: 1,
      paddingHorizontal: 10,
  
    },
  
  });

//make this component available to the app
export default up_danhmuc;
