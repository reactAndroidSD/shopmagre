import React, { Component } from 'react';
import {Select, Option} from "react-native-chooser";
import { Alert, TouchableOpacity, View, ActivityIndicator, StyleSheet, Text, FlatList, ScrollView, TextInput, Button, AppRegistry } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
export default class up_sanpham extends Component {
    static navigationOptions = {
        title: 'Cập nhật sản phẩm',
    };
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            tensp: '',
            mavach: '',
            id_danhmuc: '',
            giavon: '',
            giaban: '',
            giabuon: '',
            mausac: '',
            trongluong: '',
            baohanh: '',
            trangthai:'',
            id_nhasanxuat: '',
            mota: '',
            value: "Chọn trạng thái",
            result : this.props.navigation.state.params.idsp
        }
    }

    componentDidMount() {
      //Alert.alert(this.state.hello.toString())
     let data_send2 = {
       method: 'POST',
       body: JSON.stringify({
         ip_sanpham:this.state.result,
       }),
       headers: {
         Accept: 'application/json',
         'Content-Type': 'application/json',
       }
     }
     fetch('http://192.168.1.215:3000/chitietsanpham_byid', data_send2)
       .then((response) => response.json())
       .then((responseJson) => {
        responseJson.map(data1 => (
          this.setState({
            tensp: data1.ten_sp,
            mavach: data1.mavach,
            id_danhmuc:data1.id_danhmuc,
            giavon: data1.giavon,
            giaban: data1.giaban,
            giabuon: data1.giabuon,
            mausac:data1.mausac,
            trangthai : data1.trangthai,
            trongluong: data1.trongluong,
            baohanh: data1.baohanh,
            id_nhasanxuat: data1.id_nhasanxuat,
            mota:data1.mota
            })
        )),
         this.setState({
          //  isLoading: false,
           data: responseJson,
           
         }, function () {
            
         });
 
       })
       .catch((error) => {
         console.error(error);
       });
      }


    onSelect(value, label) {
        this.setState({ trangthai: value });
    }

    componentDidMount1() {
        if (this.state.tensp == "") {
            Alert.alert("Vui lòng nhập tên sản phẩm");
        }
        else if (this.state.id_danhmuc == "") {
            Alert.alert("Vui lòng nhập tên danh mục");
        }
        else if (this.state.mavach == "") {
            Alert.alert("Vui lòng nhập mã vạch");
        }
        else if (this.state.giavon == "") {
            Alert.alert("Vui lòng nhập giá vốn");
        }
        else if (this.state.giaban == "") {
            Alert.alert("Vui lòng nhập giá bán");
        }
        else if (this.state.trangthai == "") {
            Alert.alert("Vui lòng nhập trạng thái");
        }
        else if (this.state.giabuon == "") {
            Alert.alert("Vui lòng nhập giá buôn");
        }
        else if (this.state.mausac == "") {
            Alert.alert("Vui lòng nhập màu sắc");
        }
        else if (this.state.trongluong == "") {
            Alert.alert("Vui lòng nhập trọng lượng");
        }
        else if (this.state.baohanh == "") {
            Alert.alert("Vui lòng nhập bảo hành");
        }
        else if (this.state.id_nhasanxuat == "") {
            Alert.alert("Vui lòng nhập ID nhà sản xuất");
        }
        
        else {
            Alert.alert(
                'Xác nhận',
                'Bạn đã chắc chắn chưa?',
                [
                    { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    {
                        text: 'THAY ĐỔI', onPress: () => fetch('http://192.168.1.215:3000/capnhatsanpham', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                idsanpham :this.state.result,
                                tensp: this.state.tensp,
                                mavach: this.state.mavach,
                                id_danhmuc: parseInt(this.state.id_danhmuc),
                                giavon: parseInt(this.state.giavon),
                                giaban: parseInt(this.state.giaban),
                                giabuon: parseInt(this.state.giabuon),
                                mausac: this.state.mausac,
                                trangthai : this.state.trangthai,
                                trongluong: parseInt(this.state.trongluong),
                                baohanh: parseInt(this.state.baohanh),
                                id_nhasanxuat: this.state.id_nhasanxuat,
                                mota: this.state.mota,
                            }),
                        })
                    },
                ],
                { cancelable: false },
                
            )
           
        }


    }

   

    render() {
      
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    {/* <Header
                        leftComponent={
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.openDrawer()}>
                                <Image
                                    style={styles.logo}
                                    source={require('../../icons/ham.png')}
                                />
                            </TouchableOpacity>

                        }
                        centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                    /> */}

                    <ScrollView>
                    {this.state.data.map(data1 => (
                        <View>
                           
                            <Text>Tên sản phẩm: </Text>
                            <TextInput style={styles.input}
                                placeholder= {data1.ten_sp}
                                //placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(tensp) => this.setState({ tensp })}
                                value={this.state.tensp}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />

                            <Text>Mã vạch: </Text>
                            <TextInput style={styles.input}
                                placeholder={data1.mavach}
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(mavach) => this.setState({ mavach })}


                            />

                            <Text>ID danh mục: </Text>
                            <TextInput style={styles.input}
                                placeholder={data1.id_danhmuc.toString()}
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(id_danhmuc) => this.setState({ id_danhmuc })}


                            />

                            <Text>Trạng thái: </Text>
                            <Select 
                                onSelect={this.onSelect.bind(this)}
                                defaultText={this.state.trangthai}
                                style={{ borderWidth: 1, borderColor: "green" }}
                                textStyle={{}}
                                backdropStyle={{ backgroundColor: "#d3d5d6" }}
                                optionListStyle={{ backgroundColor: "#F5FCFF" }}
                            >
                                <Option value="đang bán">đang bán</Option>
                                <Option value="ngừng bán">ngừng bán</Option>

                            </Select>

                            <Text>Giá vốn: </Text>
                            <TextInput style={styles.input}
                                placeholder={data1.giavon.toString()}
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(giavon) => this.setState({ giavon })}


                            />

                            <Text>Giá bán: </Text>
                            <TextInput style={styles.input}
                                placeholder={data1.giaban.toString()}
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(giaban) => this.setState({ giaban })}


                            />

                            <Text>Giá buôn: </Text>
                            <TextInput style={styles.input}
                                placeholder={data1.giabuon.toString()}
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(giabuon) => this.setState({ giabuon })}


                            />


                            <Text>Màu sắc: </Text>
                            <TextInput style={styles.input}
                                placeholder={data1.mausac}
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(mausac) => this.setState({ mausac })}


                            />

                            <Text>Trọng lượng: </Text>
                            <TextInput style={styles.input}
                                placeholder={data1.trongluong.toString()}
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(trongluong) => this.setState({ trongluong })}


                            />

                            <Text>Bảo hành: </Text>
                            <TextInput style={styles.input}
                                placeholder={data1.baohanh.toString()}
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(baohanh) => this.setState({ baohanh })}


                            />

                            <Text>Nhà sản xuất: </Text>
                            <TextInput style={styles.input}
                                placeholder={data1.id_nhasanxuat.toString()}
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(id_nhasanxuat) => this.setState({ id_nhasanxuat })}


                            />


                            <Text>Mô tả: </Text>
                            <TextInput style={styles.input}
                                placeholder={data1.mota}
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(mota) => this.setState({ mota })}


                            />



                            <Button
                                title="THAY ĐỔI"
                                color="#ff0000"
                                onPress={() => this.componentDidMount1()}>

                            </Button>


                        </View>
                         ))}
                    </ScrollView>
                </View>
            )
        }
    }
}

// define your styles
const styles = StyleSheet.create({
    input: {
        marginBottom: 20,
        borderWidth: 1,
        paddingHorizontal: 10,

    },

});