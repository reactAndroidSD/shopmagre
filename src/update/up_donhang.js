import React, { Component } from 'react';
import { TouchableOpacity, View, ActivityIndicator, StyleSheet, Text, FlatList, Alert, ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
import {Select, Option} from "react-native-chooser";

export default class up_donhang extends Component {
    static navigationOptions = {
        title: 'Cập nhật Đơn Hàng',
    };
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            ten_KH: '',
            id_sp: '',
            // loaidon: '',
            soluong: '',
            vanchuyen: '',
            sdt: '',
            diachinhan: '',
        }
    }

    onSelect1(value1, label1) {
        this.setState({ value1: value1 });
    }

    onSelect2(value2, label2) {
        this.setState({ value2: value2 });
    }

    componentDidMount1() {
        if (this.state.ten_KH == "") {
            Alert.alert("Vui lòng nhập tên tên khách hàng");
        }
        else if (this.state.id_sp == "") {
            Alert.alert("Vui lòng nhập mã sản phẩm");
        }
        else if (this.state.soluong == "") {
            Alert.alert("Vui lòng nhập số lượng");
        }
        else if (this.state.sdt == "") {
            Alert.alert("Vui lòng nhập số điện thoại");
        }
        else if (this.state.diachinhan == "") {
            Alert.alert("Vui lòng nhập địa chỉ nhận");
        }
        else if (this.state.vanchuyen == "") {
            Alert.alert("Vui lòng nhập vận chuyển");
        }
        else {
            Alert.alert(
                'Xác nhận',
                'Bạn đã chắc chắn chưa?',
                [
                    { text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    {
                        text: 'OK', onPress: () => fetch('http://192.168.1.215:3000/themdonhang', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                ten_KH: this.state.ten_KH,
                                id_sp: parseInt(this.state.id_sp),
                                // loaidon: this.state.loaidon,
                                soluong: parseInt(this.state.soluong),
                                vanchuyen: parseInt(this.state.vanchuyen),
                                sdt: this.state.sdt,
                                diachinhan: this.state.diachinhan,
                                // trangthai: this.state.trangthai,
                            }),
                        })
                    },
                ],
                { cancelable: false }


            )

        }

    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    {/* <Header
                        leftComponent={
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.openDrawer()}>
                                <Image
                                    style={styles.logo}
                                    source={require('../../icons/ham.png')}
                                />
                            </TouchableOpacity>

                        }
                        centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
                    /> */}

                    <ScrollView>
                        <View>
                            <Text>Tên khách hàng: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                style={styles.input}
                                onChangeText={(ten_KH) => this.setState({ ten_KH })}


                            />

                            <Text>Sản phẩm: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(id_sp) => this.setState({ id_sp })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                style={styles.input}
                                autoCorrect={false}
                            />

                            {/* <Text>Loại đơn: </Text>
                            <Select
                                onSelect1={this.onSelect1.bind(this)}
                                defaultText={this.state.value1}
                                style={{ borderWidth: 1, borderColor: "green" }}
                                textStyle={{}}
                                backdropStyle={{ backgroundColor: "#d3d5d6" }}
                                optionListStyle={{ backgroundColor: "#F5FCFF" }}
                            >
                                <Option value1="online">online</Option>
                                <Option value1="offline">offline</Option>

                            </Select> */}

                            <Text>Số lượng: </Text>
                            <TextInput
                                style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(soluong) => this.setState({ soluong })}


                            />

                            <Text>Vận chuyển: </Text>
                            <TextInput
                                style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(vanchuyen) => this.setState({ vanchuyen })}


                            />

                            <Text>Số điện thoại: </Text>
                            <TextInput
                                style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType='phone-pad'
                                onChangeText={(sdt) => this.setState({ sdt })}


                            />

                            <Text>Địa chỉ cá nhân: </Text>
                            <TextInput
                                style={styles.input}
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(diachinhan) => this.setState({ diachinhan })}


                            />
{/* 
                            <Text>Trạng thái: </Text>
                            <Select
                                onSelect2={this.onSelect2.bind(this)}
                                defaultText={this.state.value2}
                                style={{ borderWidth: 1, borderColor: "green" }}
                                textStyle={{}}
                                backdropStyle={{ backgroundColor: "#d3d5d6" }}
                                optionListStyle={{ backgroundColor: "#F5FCFF" }}
                            >
                                <Option value2="Đang vận chuyển">Đang vận chuyển</Option>
                                <Option value2="Đã thanh toán">Đã thanh toán</Option>
                                <Option value2="Đã Huỷ">Đã Huỷ</Option>

                            </Select> */}




                            <Button
                                title="ADD"
                                color="#ff0000"
                                onPress={() => this.componentDidMount1()}>

                            </Button>


                        </View>
                    </ScrollView>
                </View>
            )
        }
    }
}

// define your styles
const styles = StyleSheet.create({
    input: {
        marginBottom: 20,
        borderWidth: 1,
        paddingHorizontal: 10,

    },

});