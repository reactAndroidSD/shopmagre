import React, { Component } from 'react';
 
import { StyleSheet, Button, Platform, View, Text, Image, TouchableOpacity, YellowBox } from 'react-native';

import { DrawerNavigator } from 'react-navigation';
import { DrawerActions } from 'react-navigation';

import { createStackNavigator, createAppContainer, createDrawerNavigator  } from 'react-navigation'
import { Slider } from 'react-native-gesture-handler';
import { red } from 'ansi-colors';

class MyHomeScreen extends React.Component {
  static navigationOptions = {
    drawerLabel: 'Home',
    drawerIcon: ({
        tintColor
    }) => ( <
        Image source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
        style = {
            [styles.icon, {
                tintColor: tintColor
            }]
        }
        />
    ),
};

  render() {
      return ( <
          Button onPress = {
              () => openDrawer = () => {
                this.props.navigation.dispatch(DrawerActions.openDrawer());
              }
          }
          title = "Go to notifications huhu" /
          >
      );
  }
}

class MyNotificationsScreen extends React.Component {
  static navigationOptions = {
      drawerLabel: 'Notifications',
      
      drawerIcon: ({
          tintColor
      }) => ( <
          Image source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
        
          />
      ),
  };

  render() {
      return ( <
          Button onPress = {
              () => this.props.navigation.goBack()
              // () => this.props.navigation.navigate('Home')
          }
          title = "Go back home" /
          >,
          <Button onPress = {
            () => this.props.navigation.navigate('DrawerOpen')
          }
          title="Drawer Open"
          />
      );
  }
}

const styles = StyleSheet.create({
  icon: {
      width: 100,
      height: 100,
  },
});

const MyApp = createDrawerNavigator({
  Home: {
      screen: MyHomeScreen,
      
  },
  Notifications: {
      screen: MyNotificationsScreen,
      
  },
},
  {
    drawerPosition: 'right',
    drawerBackgroundColor: 'red',
    drawerWidth: 200,
    initialRouteName: 'Home'
  }

);

export default createAppContainer(MyApp);

// const MyApp = createStackNavigator({
//   Home: {
//       screen: MyHomeScreen,
//   },
//   Notifications: {
//       screen: MyNotificationsScreen,
//   },
// });


// export default class App extends React.Component {
// 	render() {
//     return(
// 		<View>
//       <Text> Hi test</Text>
//       <MyApp />

//     </View>
      
//     );
// 	}
// }


// class HomeScreen extends React.Component {
//   render() {
//     const { navigate } = this.props.navigation;

//     return (
//       <View>
//         <Text>This is the home screen of the app</Text>
//         <Button
//           onPress={() => navigate('Profile', { name: 'Brent' })}
//           title="Go to Brent's profile"
//         />
//       </View>
//     );
//   }
// }

// export default class App extends React.Component {
// 	render() {
//     return(
// 		<View>
//       <Text> Hi test</Text>
//       <HomeScreen />

//     </View>
      
//     );
// 	}
// }


// import React, { Component } from 'react';
 
// import { StyleSheet, Platform, View, Text, Image, TouchableOpacity, YellowBox } from 'react-native';

// import { DrawerNavigator } from 'react-navigation';

// import { createStackNavigator, createAppContainer   } from 'react-navigation'

// class HamburgerIcon extends Component {

//   toggleDrawer=()=>{

//     console.log(this.props.navigationProps);
    
//     this.props.navigationProps.toggleDrawer();

//   }
 
//   render() {
 
//     return (
 
//       <View style={{flexDirection: 'row'}}>
 
//         <TouchableOpacity onPress={this.toggleDrawer.bind(this)} >

//           <Image
//             source={{uri : 'https://reactnativecode.com/wp-content/uploads/2018/04/hamburger_icon.png'}}
//             style={{ width: 25, height: 25, marginLeft: 5}}
//           />

//         </TouchableOpacity>
 
//       </View>
    
//     );
  
  
//   }
// }
 
// class MainActivity extends Component {

//   constructor(props) {
 
//     super(props);
  
//     YellowBox.ignoreWarnings([
//      'Warning: componentWillMount is deprecated',
//      'Warning: componentWillReceiveProps is deprecated',
//    ]);
  
//   }
 
//    render()
//    {
//       return(
 
//          <View style = { styles.MainContainer }>
 
//             <Text style={{fontSize: 23}}> This is Activity - 1 </Text>
          
//          </View>
//       );
//    }
// }

// class SecondActivity extends Component {

//   constructor(props) {
 
//     super(props);
  
//     YellowBox.ignoreWarnings([
//      'Warning: componentWillMount is deprecated',
//      'Warning: componentWillReceiveProps is deprecated',
//    ]);
  
//   }
   
//      render()
//      {
//         return(
   
//            <View style = { styles.MainContainer }>
   
//               <Text style={{fontSize: 23}}> This is Activity - 2 </Text>
            
//            </View>
//         );
//      }
//   }

//   class ThirdActivity extends Component {

//     constructor(props) {
 
//       super(props);
    
//       YellowBox.ignoreWarnings([
//        'Warning: componentWillMount is deprecated',
//        'Warning: componentWillReceiveProps is deprecated',
//      ]);
    
//     }
 
//        render()
//        {
//           return(
     
//              <View style = { styles.MainContainer }>
     
//                 <Text style={{fontSize: 23}}> This is Activity - 3 </Text>
              
//              </View>
//           );
//        }
//     }
 
//     const FirstActivity_StackNavigator = createStackNavigator ({
//       First: { 
//         screen: MainActivity, 
//         navigationOptions: ({ navigation }) => ({
//           title: 'MainActivity',
//           headerLeft : <HamburgerIcon navigationProps={ navigation }/>,

//           headerStyle: {
//             backgroundColor: '#FF9800'
//           },
//           headerTintColor: '#fff',
//         })
//       },
//     });


//     const SecondActivity_StackNavigator = createStackNavigator ({
//       Second: { 
//         screen: SecondActivity, 
//         navigationOptions: ({ navigation }) => ({
//           title: 'SecondActivity',
//           headerLeft : <HamburgerIcon navigationProps={ navigation }/>,

//           headerStyle: {
//             backgroundColor: '#FF9800'
//           },
//           headerTintColor: '#fff',
//         })
//       },
//     });


//     const ThirdActivity_StackNavigator = createStackNavigator ({
//       Third: { 
//         screen: ThirdActivity, 
//         navigationOptions: ({ navigation }) => ({
//           title: 'ThirdActivity',
//           headerLeft : <HamburgerIcon navigationProps={ navigation }/>,

//           headerStyle: {
//             backgroundColor: '#FF9800'
//           },
//           headerTintColor: '#fff',
//         })
//       },
//     });
    
// export default MyStackNavigator = createStackNavigator({
//   MainStack: { 
//     screen: FirstActivity_StackNavigator
//   },

//   SecondStack: { 
//     screen: SecondActivity_StackNavigator
//   },

//   ThirdStack: { 
//     screen: ThirdActivity_StackNavigator
//   }
// });

    
// const styles = StyleSheet.create({
    
//  MainContainer :{
 
//   flex:1,
//   paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
//   alignItems: 'center',
//   justifyContent: 'center',
    
//   }

// });



// import React from 'react';
// import { Button, Text, View, StyleSheet } from 'react-native';
// import { createDrawerNavigator, createAppContainer} from 'react-navigation';
// import { DrawerNavigator, DrawerItems } from 'react-navigation';


// class MyHomeScreen extends React.Component {
//   static navigationOptions = {
//     drawerLabel: 'Home',
//   };

//   render() {
//     return (
//       <Button
//         onPress={() => this.props.navigation.navigate('Notifications')}
//         title="Go to notifications"
//       />
//     );
//   }
// }

// class MyNotificationsScreen extends React.Component {
//   static navigationOptions = {
//     drawerLabel: 'Notifications',
   
//   };

//   render() {
//     return (
//       <Button
//         onPress={() => this.props.navigation.goBack()}
//         title="Go back home"
//       />
//     );
//   }
// }

// const MyDrawerNavigator = createDrawerNavigator({
//   Home: {
//     screen: MyHomeScreen,
//   },
//   Notifications: {
//     screen: MyNotificationsScreen,
//   },
// });

// const MyApp = createAppContainer(MyDrawerNavigator);

// export default class App extends React.Component {
// 	render() {
//     return(
// 		<View>
//       <Text> Hi test</Text>

//     </View>
      
//     );
// 	}
// }

// const RootRoute = DrawerNavigator(
//   {
//     MainDrawer: {
//       screen: MainDrawer,
//     },
//   },
//   {
//     navigationOptions: {
//     },
//     drawerPosition: 'right',
//     drawerWidth: Dimensions.get('window').width,
//     contentComponent: props => <RightSideMenu {...props} />,
//     drawerOpenRoute: 'RightSideMenu',
//     drawerCloseRoute: 'RightSideMenuClose',
//     drawerToggleRoute: 'RightSideMenuToggle',
//   },
// );

// const App = DrawerNavigator({
//   Home: {
//     screen: MyHomeScreen,
//   },
//   Notifications: {
//     screen: MyNotificationsScreen,
//   },
// });


// // RootDrawer containing drawers for each components
// const RootDrawer = DrawerNavigator(
// 	{
// 		Home: {
// 			screen: DefaultScreen,
// 		},
// 		// Register screens of all options of child components
// 		...screenMapping,
// 	},
// 	{
// 		// Custom rendering component of drawer panel
// 		contentComponent: MainDrawer,
// 	}
// );

// export default class App extends React.Component {
// 	render() {
// 		return <RootDrawer />;
// 	}
// }

//DrawerNavigator(RouteConfigs, DrawerNavigatorConfig)



// class MyHomeScreen extends React.Component {
//   static navigationOptions = {
//     drawerLabel: 'Home',
//     drawerIcon: ({ tintColor }) => (
//       <View style={{ flex: 1, justifyContent: 'center', alignItems: 'stretch' }}>
//         <Text>Home!</Text>

//         {/* <Button
//         onPress = {() => this.props.navigation.openDrawer()}
//         title ="open Drawer"
//       /> */}
        

//       </View>
//     ),
//   };

//   render() {
//     return (
//       <Button
//         onPress={() => this.props.navigation.navigate('Notifications')}
//         title="Go to notifications"
//       />

      
//     );
//   }
  
// }

// class MyNotificationsScreen extends React.Component {
//   static navigationOptions = {
//     drawerLabel: 'Notifications',
//     drawerIcon: ({ tintColor }) => (
//       <View style={{ flex: 2, justifyContent: 'center', alignItems: 'stretch' }}>
//       <Text>Home!</Text>
//     </View>
//     ),
//   };

//   render() {
//     return (
//       <Button
//         onPress={() => this.props.navigation.goBack()}
//         title="Go back home"
//       />
//     );
//   }
// }

// const styles = StyleSheet.create({
//   icon: {
//     width: 24,
//     height: 24,
//   },
// });

// const MyDrawerNavigator = createDrawerNavigator({
//   Home: {
//     screen: MyHomeScreen,
//   },
//   Notifications: {
//     screen: MyNotificationsScreen,
//   },
// });

// const MyApp = createAppContainer(MyDrawerNavigator);

// export default createAppContainer(MyDrawerNavigator);

/*******************************************************************/




// import React from 'react';
// import { Button, Text, View, StyleSheet } from 'react-native';
// import { 
//   createBottomTabNavigator,
//   createStackNavigator,
//   createAppContainer,
//   createDrawerNavigator, } from 'react-navigation';

//   class DetailsScreen extends React.Component {
//     render() {
//       return (
//         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//           <Text>Details!</Text>
//         </View>
//       );
//     }
//   }

// class HomeScreen extends React.Component {
//   render() {
//     return (
//       <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//         <Text>Home!</Text>
//         <Button
//           title="Di den setting"
//           onPress={() => this.props.navigation.navigate('Settings')}
//         />
//       </View>
//     );
//   }
// }

// class MyHomeScreen extends React.Component {
//   static navigationOptions = {
//     drawerLabel: 'Home',
//     drawerIcon: ({ tintColor }) => (
//       <Image
//         source={require('./icons/account_.svg')}
//         style={[styles.icon, {tintColor: tintColor}]}
//       />
//     ),
//   };

//   render() {
//     return (
//       <Button
//         onPress={() => this.props.navigation.navigate('Notifications')}
//         title="Go to notifications"
//       />
//     );
//   }
// }

// class MyNotificationsScreen extends React.Component {
//   static navigationOptions = {
//     drawerLabel: 'Notifications',
//     drawerIcon: ({ tintColor }) => (
//       <Image
//         source={require('./icons/account_.svg')}
//         style={[styles.icon, {tintColor: tintColor}]}
//       />
//     ),
//   };

//   render() {
//     return (
//       <Button
//         onPress={() => this.props.navigation.goBack()}
//         title="Go back home"
//       />
//     );
//   }
// }

// const styles = StyleSheet.create({
//   icon: {
//     width: 24,
//     height: 24,
//   },
// });

// const MyDrawerNavigator = createDrawerNavigator({
//   Home: {
//     screen: MyHomeScreen,
//   },
//   Notifications: {
//     screen: MyNotificationsScreen,
//   },
// });

// const MyApp = createAppContainer(MyDrawerNavigator);

// class SettingsScreen extends React.Component {
//   render() {
//     return (
//       <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//         <Text>Settings!</Text>
//         <Button
//           title="Di den trang chu"
//           onPress={() => this.props.navigation.navigate('Home')}
//         />
//       </View>
//     );
//   }
// }

// const HomeStack = createStackNavigator({
//   Home: HomeScreen,
//   Details: DetailsScreen,
// });

// const SettingsStack = createStackNavigator({
//   Settings: SettingsScreen,
//   Details: DetailsScreen,
// });

// const TabNavigator = createBottomTabNavigator({
//   Home: HomeScreen,
//   Settings: SettingsScreen,
// });


// export default createAppContainer(TabNavigator);