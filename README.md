# ShopMagRe

Ứng dụng được chạy bằng React Native nên trước hết cần phải có các yêu cầu sau:
PHẦN CỨNG: 
- Máy tính có cấu hình mạnh, 
- Tối thiểu 8Gb RAM để chạy máy ảo. 
- Vi xử lý Intel i5 đời 5 trở lên
- Một máy android chạy Android 7.1 trở lên.

PHẦN MỀM: 
- Cần có Nodejs (https://nodejs.org/en/)
- Cần có Git để giao diện làm việc trực quan hơn
- Một IDE để soạn thảo (Visual Code, Notepad++, VIM,..)
- Android Studio, việc cài Android studio sẽ cài sẵn luôn cả SDK lẫn JDK cũng như máy ảo. (Yêu cầu set path sau khi cài đặt). 
- Cài đặt máy ảo ngoài (vd: Bluestack, Genny Motion) hoặc có thể sử dụng máy android kết nối trực tiếp.
- Cài đặt npm bằng lệnh: npm install npm -g (yêu cầu nodejs đã cài đặt thành công). Hoặc cài đặt thêm Yarn.
- Cài đặt Python.
- Database có thể lựa chọn MySQL và các database truyền thống nhưng vì hiện nay xu hướng mọi người chuộng MongoDB và Firebase, nên bạn có thể sử dụng và cài đặt luôn cả MongoDB.

Cài đặt React Native:
Kiểu truyền thống: npm install -g react-native-cli
Sử dụng Expo: npm install -g expo-cli

Clone repository này: 
Mở Git Bash tại thư mục đã chọn, nhập lệnh "git clone https://reactAndroidSD@bitbucket.org/reactAndroidSD/shopmagre.git" để clone về.

Các thư viện cần có:
npm i react-native-action-button --save
react-native link react-native-vector-icons
npm install --save react-navigation
yarn add react-native-elements@beta
npm install --save react-native-gesture-handler
npm install react-native-vector-icons --save
npm install --save react-native-calendars
npm install react-native-datepicker --save

Các chạy, mở Git bash/cmd trong thư mục chứa ShopMagRe, sau đó gõ lệnh:
react-native run-android
{yêu cầu máy ảo android đã mở trước, server mongoDB đã có và phải thay địa chỉ IP trong file}
